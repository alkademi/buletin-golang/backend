# IF3250_2022_16_BULETIN_BACKEND

## Dependencies
1. Go 1.17
2. PostgreSQL 14
3. Docker 20
4. Golang-migrate latest version, [Installation guide](https://github.com/golang-migrate/migrate/tree/master/cmd/migrate)

## How To Use
1. Clone this repo
2. Open in VSCode or your favorite IDE
3. Open a new terminal that can use sh command (eg. Git Bash)
4. Run `sh scripts/run-dev.sh`
5. Run `sh scripts/migrate-up.sh` (**Make sure you have already installed golang-migrate or migrate**)
6. The server is run on localhost:8080