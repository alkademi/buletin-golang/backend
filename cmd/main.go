package main

import (
	"log"
	"net/http"

	"github.com/go-chi/cors"
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/routes"
	_ "gorm.io/driver/postgres"
	_ "gorm.io/gorm"
)

func main() {
	r := mux.NewRouter()

	err := database.ConnectDB()
	if err != nil {
		log.Fatal("Error when connecting to database")
	}

	log.Println("Success when connect to database")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "DELETE", "PUT", "OPTIONS"},
		AllowedHeaders:   []string{"Origin", "Content-Length", "Content-Type", "Authorization", "Access-Control-Allow-Origin", "Accept"},
		AllowCredentials: true,
	})

	routes.UserRoutesGroup(r)
	routes.VideoRoutesGroup(r)
	routes.VideoViewRoutesGroup(r)
	routes.ChannelRoutesGroup(r)
	routes.PlaylistRoutesGroup(r)
	routes.CategoryRoutesGroup(r)
	routes.InterestRoutesGroup(r)

	handler := c.Handler(r)

	log.Println("Connected to port http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", handler))
}
