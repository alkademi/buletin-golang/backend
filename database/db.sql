--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9
-- Dumped by pg_dump version 12.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account (
    account_id bigint NOT NULL,
    account_email character varying NOT NULL,
    account_hashed_password character varying NOT NULL,
    account_fullname character varying NOT NULL,
    account_phone_number character varying NOT NULL,
    account_profession character varying NOT NULL,
    is_admin boolean NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.account OWNER TO postgres;

--
-- Name: account_account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_account_id_seq OWNER TO postgres;

--
-- Name: account_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.account_account_id_seq OWNED BY public.account.account_id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    category_id bigint NOT NULL,
    category_name character varying NOT NULL
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_category_id_seq OWNER TO postgres;

--
-- Name: category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_category_id_seq OWNED BY public.category.category_id;


--
-- Name: channel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.channel (
    channel_id bigint NOT NULL,
    category_id bigint NOT NULL,
    channel_name character varying NOT NULL,
    channel_picture character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.channel OWNER TO postgres;

--
-- Name: channel_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.channel_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.channel_category_id_seq OWNER TO postgres;

--
-- Name: channel_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.channel_category_id_seq OWNED BY public.channel.category_id;


--
-- Name: channel_channel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.channel_channel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.channel_channel_id_seq OWNER TO postgres;

--
-- Name: channel_channel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.channel_channel_id_seq OWNED BY public.channel.channel_id;


--
-- Name: playlist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.playlist (
    playlist_id bigint NOT NULL,
    playlist_name character varying NOT NULL
);


ALTER TABLE public.playlist OWNER TO postgres;

--
-- Name: playlist_playlist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.playlist_playlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.playlist_playlist_id_seq OWNER TO postgres;

--
-- Name: playlist_playlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.playlist_playlist_id_seq OWNED BY public.playlist.playlist_id;


--
-- Name: video; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.video (
    video_id bigint NOT NULL,
    channel_id bigint NOT NULL,
    video_title character varying NOT NULL,
    video_desc character varying NOT NULL,
    video_url character varying NOT NULL,
    video_view_count integer NOT NULL,
    video_thumbnail character varying NOT NULL,
    date_posted timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.video OWNER TO postgres;

--
-- Name: video_channel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.video_channel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_channel_id_seq OWNER TO postgres;

--
-- Name: video_channel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.video_channel_id_seq OWNED BY public.video.channel_id;


--
-- Name: video_playlist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.video_playlist (
    video_id bigint NOT NULL,
    playlist_id bigint NOT NULL
);


ALTER TABLE public.video_playlist OWNER TO postgres;

--
-- Name: video_playlist_playlist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.video_playlist_playlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_playlist_playlist_id_seq OWNER TO postgres;

--
-- Name: video_playlist_playlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.video_playlist_playlist_id_seq OWNED BY public.video_playlist.playlist_id;


--
-- Name: video_playlist_video_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.video_playlist_video_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_playlist_video_id_seq OWNER TO postgres;

--
-- Name: video_playlist_video_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.video_playlist_video_id_seq OWNED BY public.video_playlist.video_id;


--
-- Name: video_video_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.video_video_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.video_video_id_seq OWNER TO postgres;

--
-- Name: video_video_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.video_video_id_seq OWNED BY public.video.video_id;


--
-- Name: account account_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account ALTER COLUMN account_id SET DEFAULT nextval('public.account_account_id_seq'::regclass);


--
-- Name: category category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN category_id SET DEFAULT nextval('public.category_category_id_seq'::regclass);


--
-- Name: channel channel_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel ALTER COLUMN channel_id SET DEFAULT nextval('public.channel_channel_id_seq'::regclass);


--
-- Name: channel category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel ALTER COLUMN category_id SET DEFAULT nextval('public.channel_category_id_seq'::regclass);


--
-- Name: playlist playlist_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playlist ALTER COLUMN playlist_id SET DEFAULT nextval('public.playlist_playlist_id_seq'::regclass);


--
-- Name: video video_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video ALTER COLUMN video_id SET DEFAULT nextval('public.video_video_id_seq'::regclass);


--
-- Name: video channel_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video ALTER COLUMN channel_id SET DEFAULT nextval('public.video_channel_id_seq'::regclass);


--
-- Name: video_playlist video_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video_playlist ALTER COLUMN video_id SET DEFAULT nextval('public.video_playlist_video_id_seq'::regclass);


--
-- Name: video_playlist playlist_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video_playlist ALTER COLUMN playlist_id SET DEFAULT nextval('public.video_playlist_playlist_id_seq'::regclass);


--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.account (account_id, account_email, account_hashed_password, account_fullname, account_phone_number, account_profession, is_admin, created_at) FROM stdin;
\.


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (category_id, category_name) FROM stdin;
1	Entertainment
2	Stories
3	Talks
4	Sport
5	Classroom
\.


--
-- Data for Name: channel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.channel (channel_id, category_id, channel_name, channel_picture, created_at) FROM stdin;
1	1	Buletin Official	placeholder	2022-01-01 12:38:39.583772+07
2	2	Bahas Apapun Channel	placeholder	2022-02-01 12:38:39.583772+07
\.


--
-- Data for Name: playlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.playlist (playlist_id, playlist_name) FROM stdin;
1	New Release
2	Best Videos
3	Inspirasi Bisnis
\.


--
-- Data for Name: video; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.video (video_id, channel_id, video_title, video_desc, video_url, video_view_count, video_thumbnail, date_posted) FROM stdin;
1	1	Guru Besar IPB Membahas Sawit	Ini adalah video terbaru	https://www.youtube.com/watch?v=DufYXWGJhv8	100	placeholder	2022-01-10 12:38:39.583772+07
2	2	Petaka Penuhnya Rumah Sakit COVID-19 di Ibu Kota	Ini adalah video tentang covid	https://www.youtube.com/watch?v=o1cLDdNiHLI	200	placeholder	2022-01-12 12:38:39.583772+07
3	1	Untuk Suporter Sepak Bola Indonesia	Sepak bola yang penuh kebusukan ini tidak layak membuat kita semua saling bertengkar, saling caci bahkan saling bantai. Kita semua, para suporter ini, adalah korban.	https://www.youtube.com/watch?v=2WPIKcIlbIE	120	placeholder	2022-01-12 12:38:39.583772+07
4	2	Petaka Penuhnya Rumah Sakit COVID-19 di Ibu Kota	Sebelum atau sesudah live Mata Najwa seperti biasa selalu banyak pendapat netizen membanjiri kolom komentar akun media sosial Mata Najwa.	https://www.youtube.com/watch?v=o1cLDdNiHLI	200	placeholder	2022-01-12 12:38:39.583772+07
5	1	Obsesi Joko Anwar Saingi Marvel	Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.	https://www.youtube.com/watch?v=RKueSD3gLJQ&t=15s	1022	placeholder	2022-01-12 12:38:39.583772+07
6	1	Jatuh Bangun Founder Tiket.com	Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.	https://www.youtube.com/watch?v=NWfmX6zqJgE	2112	placeholder	2022-01-12 12:38:39.583772+07
\.


--
-- Data for Name: video_playlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.video_playlist (video_id, playlist_id) FROM stdin;
1	2
2	1
4	2
6	3
\.


--
-- Name: account_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_account_id_seq', 1, false);


--
-- Name: category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_category_id_seq', 5, true);


--
-- Name: channel_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.channel_category_id_seq', 1, false);


--
-- Name: channel_channel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.channel_channel_id_seq', 2, true);


--
-- Name: playlist_playlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.playlist_playlist_id_seq', 3, true);


--
-- Name: video_channel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.video_channel_id_seq', 1, false);


--
-- Name: video_playlist_playlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.video_playlist_playlist_id_seq', 1, false);


--
-- Name: video_playlist_video_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.video_playlist_video_id_seq', 1, false);


--
-- Name: video_video_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.video_video_id_seq', 6, true);


--
-- Name: account account_account_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_account_email_key UNIQUE (account_email);


--
-- Name: account account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (account_id);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category_id);


--
-- Name: channel channel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel
    ADD CONSTRAINT channel_pkey PRIMARY KEY (channel_id);


--
-- Name: playlist playlist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.playlist
    ADD CONSTRAINT playlist_pkey PRIMARY KEY (playlist_id);


--
-- Name: video video_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video
    ADD CONSTRAINT video_pkey PRIMARY KEY (video_id);


--
-- Name: video_playlist video_playlist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video_playlist
    ADD CONSTRAINT video_playlist_pkey PRIMARY KEY (video_id, playlist_id);


--
-- Name: channel channel_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.channel
    ADD CONSTRAINT channel_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.category(category_id);


--
-- Name: video video_channel_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video
    ADD CONSTRAINT video_channel_id_fkey FOREIGN KEY (channel_id) REFERENCES public.channel(channel_id);


--
-- Name: video_playlist video_playlist_playlist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video_playlist
    ADD CONSTRAINT video_playlist_playlist_id_fkey FOREIGN KEY (playlist_id) REFERENCES public.playlist(playlist_id);


--
-- Name: video_playlist video_playlist_video_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.video_playlist
    ADD CONSTRAINT video_playlist_video_id_fkey FOREIGN KEY (video_id) REFERENCES public.video(video_id);


--
-- PostgreSQL database dump complete
--

