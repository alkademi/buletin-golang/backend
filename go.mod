module gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend

go 1.17

require (
	github.com/go-chi/cors v1.2.0
	github.com/golang-jwt/jwt/v4 v4.3.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.5
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)

require (
	github.com/go-test/deep v1.0.8
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.10.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.2.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.9.0 // indirect
	github.com/jackc/pgx/v4 v4.14.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/stretchr/testify v1.7.1
	golang.org/x/text v0.3.7 // indirect
)
