CREATE TABLE IF NOT EXISTS "account" (
    "account_id" BIGSERIAL PRIMARY KEY,
    "account_email" VARCHAR UNIQUE NOT NULL,
    "account_hashed_password" VARCHAR NOT NULL,
    "account_fullname" VARCHAR NOT NULL,
    "account_phone_number" VARCHAR NOT NULL,
    "account_interest_id" INTEGER[], 
    "role" VARCHAR NOT NULL,
    "created_at" timestamptz NOT NULL DEFAULT (now())
);

CREATE TABLE IF NOT EXISTS "category" (
    "category_id" BIGSERIAL NOT NULL PRIMARY KEY,
    "category_name" VARCHAR NOT NULL,
    "category_picture" VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS "channel" (
    "channel_id" BIGSERIAL NOT NULL PRIMARY KEY,
    "owner_id" BIGSERIAL NOT NULL,
    "channel_name" VARCHAR NOT NULL,
    "channel_picture" VARCHAR NOT NULL,
    "created_at" timestamptz NOT NULL DEFAULT (now()),
    FOREIGN KEY ("owner_id") REFERENCES "account" ("account_id")
    ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS "video" (
    "video_id" BIGSERIAL PRIMARY KEY,
    "channel_id" BIGSERIAL NOT NULL,
    "video_title" VARCHAR NOT NULL,
    "video_desc" VARCHAR NOT NULL,
    "video_file_id" VARCHAR NOT NULL,
    "video_thumbnail" VARCHAR NOT NULL,
    "video_interest_id" INTEGER[],
    "date_posted" timestamptz NOT NULL DEFAULT (now()),
    FOREIGN KEY ("channel_id") REFERENCES "channel" ("channel_id")
    ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS "playlist" (
    "playlist_id" BIGSERIAL NOT NULL PRIMARY KEY,
    "playlist_name" VARCHAR NOT NULL,
    "playlist_picture" VARCHAR NOT NULL,
    "category_id" BIGSERIAL NOT NULL,
    FOREIGN KEY ("category_id") REFERENCES "category" ("category_id")
    ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS "video_playlist" (
    "video_id" BIGSERIAL NOT NULL,
    "playlist_id" BIGSERIAL NOT NULL,
    PRIMARY KEY ("video_id", "playlist_id"),
    FOREIGN KEY ("video_id") REFERENCES "video" ("video_id")
    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("playlist_id") REFERENCES "playlist" ("playlist_id")
    ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS "video_view" (
    "video_id" BIGSERIAL NOT NULL,
    "viewer_id" VARCHAR NOT NULL,
    "viewed_at" timestamptz NOT NULL DEFAULT (now()),
    PRIMARY KEY ("video_id", "viewer_id"),
    FOREIGN KEY ("video_id") REFERENCES "video" ("video_id")
    ON DELETE CASCADE ON UPDATE CASCADE
);

COMMENT ON COLUMN "video_view"."viewer_id" IS 'Unique device/user identifier. Tracking will only be done once for each viewer.';

CREATE TABLE IF NOT EXISTS "interest" (
    "interest_id" BIGSERIAL NOT NULL PRIMARY KEY,
    "interest_name" VARCHAR NOT NULL
);

INSERT INTO "interest" ("interest_name") VALUES
('Education'),
('Music'),
('Sports'),
('Technology'),
('Entertainment'),
('News'),
('Business'),
('Fashion'),
('Food'),
('Travel');

INSERT INTO "account" ("account_email", "account_hashed_password", "account_fullname", "account_phone_number", "account_interest_id", "role") VALUES
('superadmin@buletin.id', '123', 'Superadmin', '08123213212', '{}', 'superadmin'),
('john@mail.com', '123', 'John Doe', '08123213312', '{3,6}', 'user'),
('jane@mail.com', '123', 'Jane Doe', '08124321131', '{1,2,3,7,8}', 'admin'),
('bob@mail.com', '123', 'Bobby Bob', '08561123122', '{}', 'user'),
('sam@mail.com', 'asdo2kD@(2938A)@9a0d', 'Sammy Sam', '08512212029', '{4}', 'user'),
('jack@mail.com', '345', 'Jack Doe', '081243211211', '{}', 'admin')
;

INSERT INTO "category" ("category_name", "category_picture") VALUES
('Entertainment', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3'),
('Stories', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3'),
('Talks', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3'),
('Sport', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3'),
('Classroom', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3')
;

INSERT INTO "channel" ("owner_id", "channel_name", "channel_picture", "created_at") VALUES
(3, 'Buletin Official', 'https://2.bp.blogspot.com/-02D2Du7X_D0/XL7ApmT9B2I/AAAAAAAAAQQ/1KNM-yNQMgsW_oBPVnYFTqF3y58RHg2pwCLcBGAs/s1600/csf.jpg', '2022-01-01 12:38:39.583772'),
(6, 'Bahas Apapun Channel', 'https://havecamerawilltravel.com/wp-content/uploads/2015/08/YouTube-Thumbnails-800x450.jpg', '2022-02-01 12:38:39.583772'),
(3, 'Mata Elang', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpFUbUeaCmfCh_uI6pNU1weUKeiuACZ1h38A&usqp=CAU', '2022-01-01 12:38:39.583772'),
(6, 'Pencinta Kucing', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJytB_ysugpHuC_YyIYTA25sueM5vvmPpXqA&usqp=CAU', '2022-02-01 12:38:39.583772'),
(3, 'Jaguar Macan', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPAHFK166bM6JP91dqFS97tGs7W1Ni5V2K-w&usqp=CAU', '2022-01-01 12:38:39.583772'),
(6, 'Suka Ketawa', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRc93j07T6avWbD24GMb0aiv0Fh8W7yy6rdGQ&usqp=CAU', '2022-02-01 12:38:39.583772')
;

INSERT INTO "video" ("channel_id", "video_title", "video_desc", "video_file_id", "video_thumbnail", "video_interest_id", "date_posted") VALUES
(1, 'Guru Besar IPB Membahas Sawit', 'Ini adalah video terbaru', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,5}', '2022-01-10 12:38:39.583772'),
(2, 'Petaka Penuhnya Rumah Sakit COVID-19 di Ibu Kota', 'Ini adalah video tentang covid', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{6}', '2022-01-12 12:38:39.583772'),
(1, 'Untuk Suporter Sepak Bola Indonesia', 'Sepak bola yang penuh kebusukan ini tidak layak membuat kita semua saling bertengkar, saling caci bahkan saling bantai. Kita semua, para suporter ini, adalah korban.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{3,5,6}', '2022-01-12 12:38:39.583772'),
(2, 'Rumah Sakit Tak Dapat Menampung Pasien COVID-19', 'Sebelum atau sesudah live Mata Najwa seperti biasa selalu banyak pendapat netizen membanjiri kolom komentar akun media sosial Mata Najwa.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3','{6}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Joko Anwar Saingi Marvel', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Bangun Founder Tiket.com', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 2', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 2', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 3', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 3', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 3', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 3', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 4', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 4', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 5', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 5', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 6', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 6', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 7', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 7', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 8', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 8', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772'),
(1, 'Obsesi Page 9', 'Joko Anwar, sutradara bertangan dingin ini bercerita tentang masa kecilnya sebagai bocah miskin yang bahkan ketika ingin menonton film di bioskop harus mengintip dari lubang ventilasi karena tidak mampu membeli tiket.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,2,4,5,6}', '2022-01-12 12:38:39.583772'),
(1, 'Jatuh Page 9', 'Punya karier bagus di luar negeri dengan penghasilan yang besar, sudah diberi status penduduk tetap di Kanada, namun lebih memilih pulang kampung dan merintis usaha dari nol? Itulah kisah Gaery Undarsa, Co-Founder sekaligus Chief Marketing Officer Tiket.com.', '1KAakN-vZtrmTtIlSBn_6NpFAUqwxoFip', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', '{1,4,6,7}', '2022-01-12 12:38:39.583772')
;

INSERT INTO "playlist" ("playlist_name", "playlist_picture", "category_id") VALUES
('New Release', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', 1),
('Best Videos', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', 1),
('Inspirasi Bisnis', '1FTLGXU0ybgddaR37ll-jTTXABOUCuS_3', 3)
;

INSERT INTO "video_playlist" VALUES
(1,2),
(2,1),
(4,2),
(6,3)
;

INSERT INTO "video_view" ("video_id", "viewer_id") VALUES
(1,'ABC-123'),
(2,'ABC-123'),
(3,'DEF-356'),
(3,'ABC-123'),
(1,'DEF-356'),
(1,'XYZ-123'),
(1,'PPP-123')
;
