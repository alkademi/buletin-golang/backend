package controllers

import (
	b64 "encoding/base64"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/util"
)

var (
	ErrId             = "ID not found"
	ErrGetUser        = "Error when getting user"
	ErrHashPassword   = "Error when hashing password"
	ErrCredentials    = "Invalid Credentials"
	ErrParseAccountID = "Error when parsing account ID"
	ErrJSONParse      = "Error when parsing body"
	ErrCreateUser     = "Error when creating user"
)

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Token string `json:"token"`
}

type RegisterRequest struct {
	Email       string `json:"email"`
	Password    string `json:"password"`
	Fullname    string `json:"fullname"`
	PhoneNumber string `json:"phone_number"`
	InterestID  string `json:"interest_id"`
	Role        string `json:"role"`
}

type RegisterResponse struct {
	Email       string `json:"email"`
	Password    string `json:"password"`
	Fullname    string `json:"fullname"`
	PhoneNumber string `json:"phone_number"`
	InterestID  string `json:"interest_id"`
	Role        string `json:"role"`
	Token       string `json:"token"`
}

type ChangePasswordRequest struct {
	Email       string `json:"email"`
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

type ForgotPasswordRequest struct {
	Email string `json:"email"`
}

type CheckResetTokenRequest struct {
	Token string `json:"token"`
}

type CheckResetTokenResponse struct {
	Email   string `json:"email"`
	Message string `json:"message"`
}

type ResetPasswordRequest struct {
	Email       string `json:"email"`
	Token       string `json:"token"`
	NewPassword string `json:"new_password"`
}

type Claims struct {
	jwt.RegisteredClaims
	Email      string `json:"email"`
	Role       string `json:"role"`
	AccountID  int    `json:"account_id"`
	InterestID string `json:"interest_id"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	var req LoginRequest
	var res LoginResponse
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrJSONParse)
		return
	}

	user, err := model.GetUserByEmail(req.Email)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if user.AccountID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.AccountHashedPassword), []byte(req.Password))
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if user.AccountID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	res.Token, err = createToken(user.AccountEmail, user.Role, user.AccountID, postgresArrayToString(user.AccountInterestID))
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	okResponse(w, res)
}

func Register(w http.ResponseWriter, r *http.Request) {
	var req RegisterRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrJSONParse)
		return
	}

	hashedPassword, err := HashPassword(req.Password)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrHashPassword)
		return
	}

	res, err := model.CreateUser(req.Email, hashedPassword, req.Fullname, req.PhoneNumber, stringToPostgresArray(req.InterestID), req.Role)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreateUser)
		return
	}

	res.AccountHashedPassword = req.Password

	if req.Role == "admin" || req.Role == "superadmin" {
		util.SendNewAdminAccountEmail(res)
	}

	if req.Role == "user" {
		token, err := createToken(res.AccountEmail, res.Role, res.AccountID, postgresArrayToString(res.AccountInterestID))
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, err.Error())
			return
		}
		data := RegisterResponse{
			Email:       res.AccountEmail,
			Password:    req.Password,
			Fullname:    res.AccountFullname,
			PhoneNumber: res.AccountPhoneNumber,
			Role:        res.Role,
			InterestID:  postgresArrayToString(res.AccountInterestID),
			Token:       token,
		}
		okResponse(w, data)
		return
	}

	okResponse(w, res)
}

func GetProfile(w http.ResponseWriter, r *http.Request) {
	Id, err := parseInt(r.Header.Get("AccountID"))
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseAccountID)
		return
	}

	res, err := model.GetUserByID(Id)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrId)
		return
	}
	okResponse(w, res)
}

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req ChangePasswordRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrJSONParse)
		return
	}

	user, err := model.GetUserByEmail(req.Email)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if user.AccountID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.AccountHashedPassword), []byte(req.OldPassword))
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	hashedPassword, err := HashPassword(req.NewPassword)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrHashPassword)
		return
	}

	_, err = model.ChangePasswordUser(req.Email, hashedPassword)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	okResponse(w, "Password Changed Successfully!")
}

func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	var req ForgotPasswordRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrJSONParse)
		return
	}

	user, err := model.GetUserByEmail(req.Email)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if user.AccountID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	resetDuration := time.Duration(5) * time.Minute
	token := user.AccountEmail + "," + user.AccountHashedPassword + "," + time.Now().Add(time.Duration(resetDuration)).Format("2006-01-02 15:04:05")
	encryptedToken := b64.StdEncoding.EncodeToString([]byte(token))

	url := "http://34.101.91.215:3000/reset?token=" + encryptedToken

	util.SendForgotPasswordEmail(user.AccountEmail, user.AccountFullname, url)

	okResponse(w, "Email Sent!")
}

func CheckTokenResetPassword(w http.ResponseWriter, r *http.Request) {
	var req CheckResetTokenRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrJSONParse)
		return
	}

	token, err := b64.StdEncoding.DecodeString(req.Token)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, "Error in decoding token")
		return
	}

	tokenInformation := strings.Split(string(token), ",")

	dateLayout := "2006-01-02 15:04:05"
	email, hashedPassword := tokenInformation[0], tokenInformation[1]
	expiredDate, err := time.Parse(dateLayout, tokenInformation[2])
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	currentTime := time.Now()
	if currentTime.After(expiredDate) {
		errorResponse(w, http.StatusBadRequest, "Token Expired")
		return
	}

	user, err := model.GetUserByEmail(email)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if user.AccountID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if hashedPassword == user.AccountHashedPassword {
		res := CheckResetTokenResponse{
			Email:   email,
			Message: "Validation Success",
		}
		okResponse(w, res)
	} else {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
	}
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	var req ResetPasswordRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrJSONParse)
		return
	}

	user, err := model.GetUserByEmail(req.Email)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	if user.AccountID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	token, err := b64.StdEncoding.DecodeString(req.Token)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, "Error in decoding token")
		return
	}

	tokenInformation := strings.Split(string(token), ",")

	hashedPassword := tokenInformation[1]

	if hashedPassword != user.AccountHashedPassword {
		errorResponse(w, http.StatusBadRequest, ErrCredentials)
		return
	}

	newHashedPassword, err := HashPassword(req.NewPassword)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrHashPassword)
		return
	}

	_, err = model.ChangePasswordUser(req.Email, newHashedPassword)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	okResponse(w, "Password Changed Successfully!")
}

func createToken(email string, role string, accID int, interestID string) (string, error) {
	loginDuration := time.Duration(1) * time.Hour
	claims := Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    os.Getenv("APPLICATION_NAME"),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Duration(loginDuration))),
		},
		Email:      email,
		Role:       role,
		AccountID:  accID,
		InterestID: interestID,
	}

	token := jwt.NewWithClaims(
		jwt.SigningMethodHS256,
		claims,
	)

	signature := os.Getenv("JWT_SIGNATURE_KEY")
	signedToken, err := token.SignedString([]byte(signature))
	if err != nil {
		return err.Error(), err
	}

	return signedToken, nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// func generatePassword(passwordLength int) string {
// 	rand.Seed(time.Now().UnixNano())
// 	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ" +
// 		"abcdefghijklmnopqrstuvwxyzåäö" +
// 		"0123456789")
// 	length := passwordLength
// 	var b strings.Builder
// 	for i := 0; i < length; i++ {
// 		b.WriteRune(chars[rand.Intn(len(chars))])
// 	}
// 	return b.String()
// }

func GetUserID(r *http.Request) int {
	userID, err := strconv.Atoi(r.Header.Get("AccountID"))
	if err != nil {
		return 0
	}
	return userID
}

func GetUserRole(r *http.Request) string {
	return r.Header.Get("Role")
}

func IsChannelOwner(userID, channelID int) bool {
	channel, err := model.DetailChannel(channelID)
	if err != nil {
		return false
	}

	return channel.OwnerID == userID
}

func IsVideoOwner(userID, videoID int) bool {
	video, err := model.DetailVideo(videoID)
	if err != nil {
		return false
	}

	return IsChannelOwner(userID, video.ChannelID)
}
