package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	ErrGetDetailCategory = "Error when getting category"
	ErrCreateCategory    = "Error when creating category: "
	ErrDeleteCategory    = "Error when deleting category: "
	ErrUpdateCategory    = "Error when updating category: "
	ErrParseCategoryName = "Error when parsing category name"
	ErrParseCategoryID   = "Error when parsing category id"
)

type CategoryRequest struct {
	CategoryName    string `json:"category_name" required:"true"`
	CategoryPicture string `json:"category_picture" required:"true"`
}

type GetCategoryResponse struct {
	CategoryID      int              `json:"category_id"`
	CategoryName    string           `json:"category_name"`
	CategoryPicture string           `json:"category_picture"`
	Playlist        []model.Playlist `json:"playlist"`
}

func ListCategory(w http.ResponseWriter, r *http.Request) {
	res, err := model.ListCategory()
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailCategory)
		return
	}
	okResponse(w, res)
}

func GetCategory(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	CategoryID := vars["CategoryID"]
	req, err = parseInt(CategoryID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseCategoryID)
		return
	}

	category, err := model.DetailCategory(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailCategory+err.Error())
		return
	}

	query := "SELECT * FROM playlist WHERE 1=1 AND category_id = ?"
	args := []interface{}{}
	args = append(args, category.CategoryID)

	playlist, err := model.ListPlaylist(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailCategory+err.Error())
		return
	}

	res := GetCategoryResponse{
		CategoryID:      category.CategoryID,
		CategoryName:    category.CategoryName,
		CategoryPicture: category.CategoryPicture,
		Playlist:        playlist,
	}
	okResponse(w, res)
}

func CreateCategory(w http.ResponseWriter, r *http.Request) {
	var req CategoryRequest
	var err error

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	category, err := model.CreateCategory(req.CategoryName, req.CategoryPicture)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreateCategory+err.Error())
		return
	}

	okResponse(w, category)
}

func UpdateCategory(w http.ResponseWriter, r *http.Request) {
	var req CategoryRequest
	var err error

	vars := mux.Vars(r)
	CategoryID := vars["CategoryID"]
	CategoryIDInt, err := parseInt(CategoryID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseCategoryID)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	category, err := model.UpdateCategory(CategoryIDInt, req.CategoryName, req.CategoryPicture)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrUpdateCategory+err.Error())
		return
	}

	okResponse(w, category)
}

func DeleteCategory(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	CategoryID := vars["CategoryID"]
	req, err = parseInt(CategoryID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseCategoryID)
		return
	}

	rowsAffected, err := model.DeleteCategory(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrDeleteCategory+err.Error())
		return
	}

	okResponse(w, "Successfully deleted "+strconv.Itoa(rowsAffected)+" category(ies)")
}
