package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	ErrGetDetailChannel = "Error when getting channel"
	ErrCreateChannel = "Error when creating channel: "
	ErrDeleteChannel = "Error when deleting channel: "
	ErrUpdateChannel = "Error when updating channel: "
	ErrParseChannelName = "Error when parsing channel name"
	ErrParseOwnerID = "Error when parsing owner id"
)

type ListChannelRequest struct {
	PageNo int `json:"page_no" required:"true"`
	PageSize int `json:"page_size" required:"true"`
	ChannelName string `json:"channel_name,omitempty"`
	OwnerID int `json:"owner_id,omitempty"`
	OrderBy string `json:"order_by,omitempty"`
	OrderByType string `json:"order_by_type,omitempty"`
}

type ListChannelResponse struct {
	Channels []model.Channel `json:"channels"`
}

func ListChannel(w http.ResponseWriter, r *http.Request) {
	var req ListChannelRequest
	var err error

	req.PageNo, err = parseIntParam(r, "page_no")
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePageNo)
		return
	}

	req.PageSize, err = parseIntParam(r, "page_size")
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePageSize)
		return
	}
	
	req.OwnerID, err = parseIntParam(r, "owner_id")
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseOwnerID)
		return
	}

	req.ChannelName = parseStringParam(r, "channel_name")
	req.OrderBy = parseStringParam(r, "order_by")
	req.OrderByType = parseStringParam(r, "order_by_type")

	query := "SELECT * FROM channel WHERE 1=1"
	args := []interface{}{}

	if req.OwnerID != 0 {
		query += " AND owner_id = ?"
		args = append(args, req.OwnerID)
	}
	if req.ChannelName != "" {
		query += " AND LOWER(channel_name) LIKE LOWER(?)"
		args = append(args, "%" + req.ChannelName + "%")
	}

	if req.OrderBy == "created_at" {
		query += " ORDER BY created_at"
	} else if req.OrderBy == "owner_id" {
		query += " ORDER BY owner_id"
	} else if req.OrderBy == "channel_name" {
		query += " ORDER BY channel_name"
	} else {
		query += " ORDER BY channel_id"
	}

	if req.OrderByType == "asc" {
		query += " ASC"
	} else {
		query += " DESC"
	}

	query += " LIMIT ? OFFSET ?"
	args = append(args, req.PageSize, (req.PageNo-1)*req.PageSize)

	channels, err := model.ListChannel(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailChannel + err.Error())
		return
	}

	res := ListChannelResponse{
		Channels: channels,
	}

	okResponse(w, res)
}

type GetChannelResponse struct {
	ChannelID      int    `json:"channel_id"`
	OwnerID 		int    `json:"owner_id"`
	ChannelName    string    `json:"channel_name"`
	ChannelPicture string    `json:"channel_picture"`
	CreatedAt      time.Time `json:"created_at"`
}

func GetChannel(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	ChannelID := vars["ChannelID"]
	req, err = parseInt(ChannelID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseChannelID)
		return
	}

	channel, err := model.DetailChannel(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailChannel + err.Error())
		return
	}

	DetailRes := GetChannelResponse{
		ChannelID:      channel.ChannelID,
		OwnerID: 		channel.OwnerID,
		ChannelName:    channel.ChannelName,
		ChannelPicture: channel.ChannelPicture,
		CreatedAt:      channel.CreatedAt,
	}

	okResponse(w, DetailRes)
}

type CreateChannelRequest struct {
	OwnerID 		int    `json:"owner_id"`
	ChannelName    string    `json:"channel_name"`
	ChannelPicture string    `json:"channel_picture"`
}

func CreateChannel(w http.ResponseWriter, r *http.Request) {
	var req CreateChannelRequest
	var err error

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	ownerID := GetUserID(r)
	channel, err := model.CreateChannel(ownerID, req.ChannelName, req.ChannelPicture)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreateChannel + err.Error())
		return
	}

	okResponse(w, channel)
}

type UpdateChannelRequest struct {
	OwnerID 		int    `json:"owner_id"`
	ChannelName    string    `json:"channel_name"`
	ChannelPicture string    `json:"channel_picture"`
}

func UpdateChannel(w http.ResponseWriter, r *http.Request) {
	var req UpdateChannelRequest
	var err error

	vars := mux.Vars(r)
	ChannelID := vars["ChannelID"]
	ChannelIDInt, err := parseInt(ChannelID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseChannelID)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	ownerID := GetUserID(r)
	if !IsChannelOwner(ownerID, ChannelIDInt) {
		errorResponse(w, http.StatusForbidden, ErrUpdateChannel + "Unauthorized")
		return
	}

	channel, err := model.UpdateChannel(ChannelIDInt, req.ChannelName, req.ChannelPicture)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrUpdateChannel + err.Error())
		return
	}

	okResponse(w, channel)
}

func DeleteChannel(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	ChannelID := vars["ChannelID"]
	req, err = parseInt(ChannelID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseChannelID + err.Error())
		return
	}

	if !IsChannelOwner(GetUserID(r), req) {
		errorResponse(w, http.StatusForbidden, ErrDeleteChannel + "Unauthorized")
		return
	}

	rowsAffected, err := model.DeleteChannel(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrDeleteChannel + err.Error())
		return
	}

	okResponse(w,  "successfully deleted " + strconv.Itoa(rowsAffected) + " channel(s)")
}
