package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
)

func intToString(i int) string {
	return strconv.Itoa(i)
}

func parseInt(str string) (int, error) {
	return strconv.Atoi(str)
}

func parseIntParam(r *http.Request, paramName string) (int, error) {
	param := r.URL.Query().Get(paramName)
	if param == "" {
		return 0, nil
	}

	return strconv.Atoi(param)
}

func parseStringParam(r *http.Request, paramName string) string {
	param := r.URL.Query().Get(paramName)
	return param
}

func stringToIntSlice(str string) []int {
	var slice []int
	strSlice := strings.Split(str, ",")
	for _, value := range strSlice {
		intValue, _ := parseInt(value)
		slice = append(slice, intValue)
	}
	return slice
}

func postgresArrayToString(postgresArray string) string {
	return postgresArray[1:len(postgresArray)-1]
}

func stringToPostgresArray(str string) string {
	return "{" + str + "}"
}

func sendResponse(w http.ResponseWriter, status int, data interface{}) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func errorResponse(w http.ResponseWriter, status int, err string) {
	sendResponse(w, status, map[string]string{
		"error": err,
	})
}

func okResponse(w http.ResponseWriter, data interface{}) {
	sendResponse(w, http.StatusOK, map[string]interface{}{
		"data": data,
	})
}
