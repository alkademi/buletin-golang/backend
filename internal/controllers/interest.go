package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	ErrListInterest = "Error when getting interest"
	ErrGetInterest	 = "Error when getting interest"
	ErrCreateInterest = "Error when creating interest"
	ErrDeleteInterest = "Error when deleting interest: "
	ErrUpdateInterest = "Error when updating interest"
)

type ListInterestResponse struct {
	Interests map[int]string `json:"interests"`
}

func ListInterest(w http.ResponseWriter, r *http.Request) {
	interestID := stringToIntSlice(parseStringParam(r, "interest_id"))

	query := "SELECT * FROM interest WHERE 1=1"
	args := []interface{}{}

	if interestID[0] != 0 {
		query += " AND interest_id IN (?)"
		args = append(args, interestID)
	}
	
	interests, err := model.ListInterest(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrListInterest)
		return
	}

	interestMap := makeInterestMap(interests)

	response := ListInterestResponse{
		Interests: interestMap,
	}

	okResponse(w, response)
}

func makeInterestMap(interests []model.Interest) map[int]string {
	interestMap := make(map[int]string)

	for _, interest := range interests {
		interestMap[interest.InterestID] = interest.InterestName
	}

	return interestMap
}

type GetInterestResponse struct {
	Interest model.Interest `json:"interest"`
}

func GetInterest(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	InterestID := vars["InterestID"]
	req, err = parseInt(InterestID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseInterestID + err.Error())
		return
	}

	interest, err := model.GetInterest(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetInterest + err.Error())
		return
	}

	response := GetInterestResponse{
		Interest: *interest,
	}

	okResponse(w, response)
}

type CreateInterestRequest struct {
	InterestName string `json:"interest_name"`
}

type CreateInterestResponse struct {
	Interest model.Interest `json:"interest"`
}

func CreateInterest(w http.ResponseWriter, r *http.Request) {
	var req CreateInterestRequest
	var err error

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	interest, err := model.CreateInterest(req.InterestName)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreateInterest)
		return
	}

	response := CreateInterestResponse{
		Interest: *interest,
	}

	okResponse(w, response)
}

type UpdateInterestRequest struct {
	InterestID   int    `json:"interest_id"`
	InterestName string `json:"interest_name"`
}

type UpdateInterestResponse struct {
	Interest model.Interest `json:"interest"`
}

func UpdateInterest(w http.ResponseWriter, r *http.Request) {
	var req UpdateInterestRequest
	var err error

	vars := mux.Vars(r)
	InterestID := vars["InterestID"]
	InterestIDint, err := parseInt(InterestID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseInterestID + err.Error())
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	interest, err := model.UpdateInterest(InterestIDint, req.InterestName)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrUpdateInterest)
		return
	}

	response := UpdateInterestResponse{
		Interest: *interest,
	}

	okResponse(w, response)
}

func DeleteInterest(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	InterestID := vars["InterestID"]
	req, err = parseInt(InterestID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseInterestID + err.Error())
		return
	}

	rowsAffected, err := model.DeleteInterest(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrDeleteInterest + err.Error())
		return
	}

	okResponse(w,  "successfully deleted " + strconv.Itoa(rowsAffected) + " interest(s)")
}
