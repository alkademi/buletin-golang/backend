package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	ErrParsePlaylistID = "Error when parsing playlist id"
	ErrGetDetailPlaylist = "Error when getting playlist: "
	ErrCreatePlaylist = "Error when creating playlist: "
	ErrDeletePlaylist = "Error when deleting playlist: "
	ErrUpdatePlaylist = "Error when updating playlist: "
	ErrParsePlaylistName = "Error when parsing playlist name"
	ErrAddVideoToPlaylist = "Error when adding video to playlist: "
	ErrDeleteVideoFromPlaylist = "Error when deleting video from playlist: "
)

type ListPlaylistRequest struct {
	PlaylistName string `json:"playlist_name,omitempty"`
	CategoryID int `json:"category_id,omitempty"`
	OrderBy string `json:"order_by,omitempty"`
	OrderByType string `json:"order_by_type,omitempty"`
}

type ListPlaylistResponse struct {
	Playlists []model.Playlist `json:"playlists"`
}

func ListPlaylist(w http.ResponseWriter, r *http.Request) {
	var req ListPlaylistRequest
	var err error

	req.CategoryID, err = parseIntParam(r, "category_id")
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePageNo)
		return
	}

	req.PlaylistName = parseStringParam(r, "playlist_name")
	req.OrderBy = parseStringParam(r, "order_by")
	req.OrderByType = parseStringParam(r, "order_by_type")

	query := "SELECT * FROM playlist WHERE 1=1"
	args := []interface{}{}

	if req.CategoryID != 0 {
		query += " AND category_id = ?"
		args = append(args, req.CategoryID)
	}
	if req.PlaylistName != "" {
		query += " AND LOWER(playlist_name) LIKE LOWER(?)"
		args = append(args, "%" + req.PlaylistName + "%")
	}

	if req.OrderBy == "playlist_name" {
		query += " ORDER BY playlist_name"
	} else if req.OrderBy == "category_id" {
		query += " ORDER BY category_id"
	} else {
		query += " ORDER BY playlist_id"
	}

	if req.OrderByType == "asc" {
		query += " ASC"
	} else {
		query += " DESC"
	}

	playlists, err := model.ListPlaylist(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailPlaylist + err.Error())
		return
	}

	res := ListPlaylistResponse{
		Playlists: playlists,
	}

	okResponse(w, res)
}

type GetPlaylistResponse struct {
	PlaylistName string `json:"playlist_name"`
	PlaylistPicture string `json:"playlist_picture"`
	PlaylistID   int    `json:"playlist_id"`
	CategoryID   int    `json:"category_id"`
}

func GetPlaylist(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	PlaylistID := vars["PlaylistID"]
	req, err = parseInt(PlaylistID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePlaylistID)
		return
	}

	playlist, err := model.DetailPlaylist(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailPlaylist + err.Error())
		return
	}

	DetailRes := GetPlaylistResponse{
		PlaylistID:      playlist.PlaylistID,
		PlaylistName:    playlist.PlaylistName,
		PlaylistPicture: playlist.PlaylistPicture,
		CategoryID:      playlist.CategoryID,
	}

	okResponse(w, DetailRes)
}

type CreatePlaylistRequest struct {
	PlaylistName string `json:"playlist_name" required:"true"`
	PlaylistPicture string `json:"playlist_picture" required:"true"`
	CategoryID   int    `json:"category_id" required:"true"`
}

func CreatePlaylist(w http.ResponseWriter, r *http.Request) {
	var req CreatePlaylistRequest
	var err error

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePlaylistName)
		return
	}

	playlist, err := model.CreatePlaylist(req.PlaylistName, req.PlaylistPicture, req.CategoryID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreatePlaylist + err.Error())
		return
	}

	okResponse(w, playlist)
}

type UpdatePlaylistRequest struct {
	PlaylistName string `json:"playlist_name"`
	PlaylistPicture string `json:"playlist_picture"`
	CategoryID   int    `json:"category_id"`
}

func UpdatePlaylist(w http.ResponseWriter, r *http.Request) {
	var req UpdatePlaylistRequest
	var err error

	vars := mux.Vars(r)
	PlaylistID := vars["PlaylistID"]
	PlaylistIDInt, err := parseInt(PlaylistID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePlaylistID)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	if req.PlaylistName == "" && req.CategoryID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}

	playlist, err := model.UpdatePlaylist(PlaylistIDInt, req.PlaylistName, req.PlaylistPicture, req.CategoryID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrUpdatePlaylist + err.Error())
		return
	}

	okResponse(w, playlist)
}

func DeletePlaylist(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	PlaylistID := vars["PlaylistID"]
	req, err = parseInt(PlaylistID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePlaylistID)
		return
	}

	rowsAffected, err := model.DeletePlaylist(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrDeletePlaylist + err.Error())
		return
	}

	okResponse(w,  "successfully deleted " + strconv.Itoa(rowsAffected) + " playlist(s)")
}

type AddVideoToPlaylistRequest struct {
	VideoID int `json:"video_id" required:"true"`
}

func AddVideoToPlaylist(w http.ResponseWriter, r *http.Request) {
	var req AddVideoToPlaylistRequest
	var err error

	vars := mux.Vars(r)
	PlaylistID := vars["PlaylistID"]
	PlaylistIDInt, err := parseInt(PlaylistID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePlaylistID)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	rowsAffected, err := model.AddVideoToPlaylist(PlaylistIDInt, req.VideoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrAddVideoToPlaylist + err.Error())
		return
	}

	if rowsAffected == 0 {
		errorResponse(w, http.StatusBadRequest, ErrAddVideoToPlaylist)
		return
	}

	okResponse(w,  "successfully added " + strconv.Itoa(rowsAffected) + " video(s) to playlist")
}

type DeleteVideoFromPlaylistRequest struct {
	VideoID int `json:"video_id" required:"true"`
}

func DeleteVideoFromPlaylist(w http.ResponseWriter, r *http.Request) {
	var req DeleteVideoFromPlaylistRequest
	var err error

	vars := mux.Vars(r)
	PlaylistID := vars["PlaylistID"]
	PlaylistIDInt, err := parseInt(PlaylistID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParsePlaylistID)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseBody)
		return
	}

	rowsAffected, err := model.DeleteVideoFromPlaylist(PlaylistIDInt, req.VideoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrDeleteVideoFromPlaylist + err.Error())
		return
	}

	okResponse(w,  "successfully deleted " + strconv.Itoa(rowsAffected) + " video(s) from playlist")
}