package controllers

import (
	"encoding/json"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	ErrParseBody             = "Error when parsing body"
	ErrParsePageNo           = "Error when parsing page_no"
	ErrParsePageSize         = "Error when parsing page_size"
	ErrParseChannelID        = "Error when parsing channel_id"
	ErrGetVideos             = "Error when getting videos"
	ErrParseVideoID          = "Error when parsing video_id"
	ErrGetDetailVideos       = "Error when getting video detail"
	ErrCreateVideo           = "Error when creating video"
	ErrUpdateVideo           = "Error when updating video"
	ErrDeleteVideo           = "Error when deleting video"
	ErrParseInterestID       = "Error when parsing interest_id"
	ErrParsePlaylistIDExcept = "Error when parsing playlist_id"
	ErrGetHotVideos          = "Error when getting hot videos"
)

type ListVideosRequest struct {
	PageNo           int    `json:"page_no" required:"true"`
	PageSize         int    `json:"page_size" required:"true"`
	ChannelID        int    `json:"channel_id,omitempty"`
	OwnerID          int    `json:"owner_id,omitempty"`
	PlaylistID       int    `json:"playlist_id,omitempty"`
	PlaylistIDExcept int    `json:"playlist_id_except,omitempty"`
	CategoryID       int    `json:"category_id,omitempty"`
	InterestID       string `json:"interest_id,omitempty"`
	OrderBy          string `json:"order_by,omitempty"`
	OrderByType      string `json:"order_by_type,omitempty"`
	Title            string `json:"title,omitempty"`
}

type ListVideosResponse struct {
	Videos []VideoDetailResponse `json:"videos"`
}

type VideoDetailResponse struct {
	VideoID           int            `json:"video_id"`
	VideoTitle        string         `json:"video_title"`
	VideoDesc         string         `json:"video_desc"`
	VideoFileID       string         `json:"video_file_id"`
	VideoViewCount    int            `json:"video_view_count"`
	VideoThumbnail    string         `json:"video_thumbnail"`
	VideoInterestID   string         `json:"video_interest_id"`
	VideoInterestInfo map[int]string `json:"video_interest_info"`
	DatePosted        time.Time      `json:"date_posted"`
	ChannelInfo       model.Channel  `json:"channel_info"`
}

type CreateVideoRequest struct {
	VideoTitle      string `json:"video_title" required:"true"`
	VideoDesc       string `json:"video_desc" required:"true"`
	VideoFileID     string `json:"video_file_id" required:"true"`
	VideoThumbnail 	string `json:"video_thumbnail" required:"true"`
	VideoInterestID string `json:"video_interest_id" required:"true"`
	ChannelID  		int    `json:"channel_id" required:"true"`
}

type UpdateVideoRequest struct {
	VideoID         int    `json:"video_id" required:"true"`
	VideoTitle      string `json:"video_title" required:"true"`
	VideoDesc       string `json:"video_desc" required:"true"`
	VideoFileID     string `json:"video_file_id" required:"true"`
	VideoThumbnail 	string `json:"video_thumbnail" required:"true"`
	VideoInterestID string `json:"video_interest_id", required:"true"`
}

func ListVideos(w http.ResponseWriter, r *http.Request) {
	var req ListVideosRequest
	var err error

	req.PageNo, err = parseIntParam(r, "page_no")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParsePageNo)
		return
	}

	req.PageSize, err = parseIntParam(r, "page_size")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParsePageSize)
		return
	}

	req.ChannelID, err = parseIntParam(r, "channel_id")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseChannelID)
		return
	}

	req.OwnerID, err = parseIntParam(r, "owner_id")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseChannelID)
		return
	}

	req.PlaylistID, err = parseIntParam(r, "playlist_id")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParsePlaylistID)
		return
	}

	req.PlaylistIDExcept, err = parseIntParam(r, "playlist_id_except")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParsePlaylistIDExcept)
		return
	}

	req.CategoryID, err = parseIntParam(r, "category_id")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseCategoryID)
		return
	}

	req.InterestID = parseStringParam(r, "interest_id")
	req.OrderBy = parseStringParam(r, "order_by")
	req.OrderByType = parseStringParam(r, "order_by_type")
	req.Title = parseStringParam(r, "title")

	query := "SELECT * FROM video WHERE 1=1"
	args := []interface{}{}

	if req.ChannelID != 0 {
		query += " AND channel_id = ?"
		args = append(args, req.ChannelID)
	}

	if req.OwnerID != 0 {
		query += " AND channel_id IN (SELECT channel_id FROM channel WHERE owner_id = ?)"
		args = append(args, req.OwnerID)
	}

	if req.PlaylistID != 0 {
		query += " AND video_id IN (SELECT video_id FROM video_playlist WHERE playlist_id = ?)"
		args = append(args, req.PlaylistID)
	}

	if req.PlaylistIDExcept != 0 {
		query += " AND video_id NOT IN (SELECT video_id FROM video_playlist WHERE playlist_id = ?)"
		args = append(args, req.PlaylistIDExcept)
	}

	if req.CategoryID != 0 {
		query += "AND video_id IN (SELECT video_id FROM video_playlist WHERE playlist_id IN (SELECT playlist_id FROM playlist WHERE category_id = ?))"
		args = append(args, req.CategoryID)
	}

	if req.InterestID != "" {
		query += " AND"
		sliceInterestID := strings.Split(req.InterestID, ",")

		for i, interestID := range sliceInterestID {
			query += " ? = ANY(video_interest_id)"

			if i != len(sliceInterestID)-1 {
				query += " OR"
			}

			interestIDInt, err := parseInt(interestID)
			if err != nil {
				errorResponse(w, http.StatusBadRequest, ErrParseInterestID)
				return
			}

			args = append(args, interestIDInt)
		}
	}

	if req.Title != "" {
		query += " AND LOWER(video_title) LIKE LOWER(?)"
		args = append(args, "%"+req.Title+"%")
	}

	if req.OrderBy == "view_count" {
		viewCountQuery := "SELECT video_id, COUNT(*) AS total_view_count FROM video_view GROUP BY video_id ORDER BY total_view_count"
		viewCountArgs := []interface{}{}

		if req.OrderByType == "desc" {
			viewCountQuery += " DESC"
		} else {
			viewCountQuery += " ASC"
		}

		if req.PageNo != 0 && req.PageSize != 0 {
			viewCountQuery += " LIMIT ? OFFSET ?"
			viewCountArgs = append(viewCountArgs, req.PageSize, (req.PageNo-1)*req.PageSize)
		}

		viewCount, err := model.ListVideoView(viewCountQuery, viewCountArgs...)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrGetVideos)
			return
		}

		query += " AND video_id IN (?)"

		var videoIDs []int
		for _, videoView := range viewCount {
			videoIDs = append(videoIDs, videoView.VideoID)
		}

		args = append(args, videoIDs)
	} else {
		if req.OrderBy == "date_posted" {
			query += " ORDER BY date_posted"
		} else if req.OrderBy == "video_title" {
			query += " ORDER BY video_title"
		} else {
			query += " ORDER BY video_id"
		}

		if req.OrderByType == "asc" {
			query += " ASC"
		} else {
			query += " DESC"
		}
	}

	query += " LIMIT ? OFFSET ?"
	args = append(args, req.PageSize, (req.PageNo-1)*req.PageSize)

	videos, err := model.ListVideos(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetVideos)
		return
	}

	var res ListVideosResponse

	interest, err := model.ListAllInterest()
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetInterest)
		return
	}

	interestMap := makeInterestMap(interest)

	for _, video := range videos {
		channel, err := model.DetailChannel(video.ChannelID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrGetDetailVideos)
			return
		}

		videoInterestInfo := make(map[int]string)
		for _, interestID := range stringToIntSlice(postgresArrayToString(video.VideoInterestID)) {
			videoInterestInfo[interestID] = interestMap[interestID]
		}

		viewCount, err := model.CountVideoView(video.VideoID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrCountVideoView)
			return
		}

		res.Videos = append(res.Videos, VideoDetailResponse{
			VideoID:           video.VideoID,
			VideoTitle:        video.VideoTitle,
			VideoDesc:         video.VideoDesc,
			VideoFileID:       video.VideoFileID,
			VideoViewCount:    viewCount,
			VideoThumbnail:    video.VideoThumbnail,
			VideoInterestID:   postgresArrayToString(video.VideoInterestID),
			VideoInterestInfo: videoInterestInfo,
			DatePosted:        video.DatePosted,
			ChannelInfo:       *channel,
		})
	}

	if req.OrderBy == "view_count" {
		if req.OrderByType == "desc" {
			sort.Slice(res.Videos, func(i, j int) bool {
				return res.Videos[i].VideoViewCount > res.Videos[j].VideoViewCount
			})
		} else {
			sort.Slice(res.Videos, func(i, j int) bool {
				return res.Videos[i].VideoViewCount < res.Videos[j].VideoViewCount
			})
		}
	}

	okResponse(w, res)
}

func ListHotVideos(w http.ResponseWriter, r *http.Request) {
	limit, err := parseIntParam(r, "limit")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrGetHotVideos)
		return
	}

	nLastDay, err := parseIntParam(r, "n_last_day")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrGetHotVideos)
		return
	}

	if limit == 0 {
		limit = 10
	}

	if nLastDay == 0 {
		nLastDay = 7
	}

	query := "SELECT video_id, COUNT(*) as total_view_count FROM video_view WHERE viewed_at BETWEEN ? AND ? GROUP BY video_id ORDER BY COUNT(*) DESC LIMIT ?"
	args := []interface{}{
		time.Now().AddDate(0, 0, -1*nLastDay),
		time.Now(),
		limit,
	}

	hotVideos, err := model.ListVideoView(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetHotVideos)
		return
	}

	var res ListVideosResponse

	interest, err := model.ListAllInterest()
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetInterest)
		return
	}

	interestMap := makeInterestMap(interest)

	for _, hotVideo := range hotVideos {
		video, err := model.DetailVideo(hotVideo.VideoID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrGetDetailVideos)
			return
		}

		videoInterestInfo := make(map[int]string)
		for _, interestID := range stringToIntSlice(postgresArrayToString(video.VideoInterestID)) {
			videoInterestInfo[interestID] = interestMap[interestID]
		}

		channel, err := model.DetailChannel(video.ChannelID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrGetDetailVideos)
			return
		}

		res.Videos = append(res.Videos, VideoDetailResponse{
			VideoID:           video.VideoID,
			VideoTitle:        video.VideoTitle,
			VideoDesc:         video.VideoDesc,
			VideoFileID:       video.VideoFileID,
			VideoThumbnail:    video.VideoThumbnail,
			VideoInterestID:   postgresArrayToString(video.VideoInterestID),
			VideoInterestInfo: videoInterestInfo,
			VideoViewCount:    hotVideo.TotalViewCount,
			DatePosted:        video.DatePosted,
			ChannelInfo:       *channel,
		})
	}

	okResponse(w, res)
}

func DetailVideo(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	VideoID := vars["VideoID"]
	req, err = parseInt(VideoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseVideoID)
		return
	}

	video, err := model.DetailVideo(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailVideos)
		return
	}

	interest, err := model.ListInterestByID(stringToIntSlice(postgresArrayToString(video.VideoInterestID)))
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetInterest)
		return
	}

	channel, err := model.DetailChannel(video.ChannelID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetDetailVideos)
		return
	}

	viewCount, err := model.CountVideoView(video.VideoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCountVideoView)
		return
	}

	var DetailRes VideoDetailResponse
	DetailRes.VideoID = video.VideoID
	DetailRes.VideoTitle = video.VideoTitle
	DetailRes.VideoDesc = video.VideoDesc
	DetailRes.VideoFileID = video.VideoFileID
	DetailRes.VideoViewCount = viewCount
	DetailRes.VideoThumbnail = video.VideoThumbnail
	DetailRes.VideoInterestID = postgresArrayToString(video.VideoInterestID)
	DetailRes.VideoInterestInfo = makeInterestMap(interest)
	DetailRes.DatePosted = video.DatePosted
	DetailRes.ChannelInfo = *channel

	okResponse(w, DetailRes)
}

func CreateVideo(w http.ResponseWriter, r *http.Request) {
	var req CreateVideoRequest
	var err error

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}

	if req.VideoTitle == "" || req.VideoDesc == "" || req.VideoFileID == "" || req.VideoThumbnail == "" || req.ChannelID == 0 {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}	

	if !IsChannelOwner(GetUserID(r), req.ChannelID) {
		errorResponse(w, http.StatusForbidden, ErrCreateVideo)
		return
	}

	video, err := model.CreateVideo(req.ChannelID, req.VideoTitle, req.VideoDesc, req.VideoFileID, req.VideoThumbnail, stringToPostgresArray(req.VideoInterestID))
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreateVideo)
		return
	}

	video.VideoInterestID = postgresArrayToString(video.VideoInterestID)

	okResponse(w, video)
}

func UpdateVideo(w http.ResponseWriter, r *http.Request) {
	var req UpdateVideoRequest
	var err error

	vars := mux.Vars(r)
	VideoID := vars["VideoID"]
	VideoIDInt, err := parseInt(VideoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseVideoID)
		return
	}
	
	if !IsVideoOwner(GetUserID(r), VideoIDInt) {
		errorResponse(w, http.StatusForbidden, ErrCreateVideo)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}

	if req.VideoTitle == "" || req.VideoDesc == "" || req.VideoFileID == "" {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}

	video, err := model.UpdateVideo(VideoIDInt, req.VideoTitle, req.VideoDesc, req.VideoFileID, req.VideoThumbnail, stringToPostgresArray(req.VideoInterestID))
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrUpdateVideo)
		return
	}

	video.VideoInterestID = postgresArrayToString(video.VideoInterestID)

	okResponse(w, video)
}

func DeleteVideo(w http.ResponseWriter, r *http.Request) {
	var req int
	var err error

	vars := mux.Vars(r)
	VideoID := vars["VideoID"]
	req, err = parseInt(VideoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrParseVideoID)
		return
	}

	if !IsVideoOwner(GetUserID(r), req) {
		errorResponse(w, http.StatusForbidden, ErrCreateVideo)
		return
	}

	rowsAffected, err := model.DeleteVideo(req)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrDeleteVideo)
		return
	}

	okResponse(w, "successfully deleted "+strconv.Itoa(rowsAffected)+" video(s)")
}
