package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	ErrCreateVideoView = "error creating video view:"
	ErrCountVideoView  = "error counting video view:"
	ErrParseQuery	   = "error parsing query:"
)

type CreateVideoViewRequest struct {
	VideoID       int       `json:"video_id" required:"true"`
	ViewerID 	  string    `json:"viewer_id" required:"true"`
}

func CreateVideoView(w http.ResponseWriter, r *http.Request) {
	var req CreateVideoViewRequest
	var err error

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}

	if req.VideoID == 0 || req.ViewerID == "" {
		errorResponse(w, http.StatusBadRequest, ErrCreateVideoView + " video id or viewer id is empty")
		return
	}

	videoView, err := model.UpsertVideoView(req.VideoID, req.ViewerID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCreateVideoView + err.Error())
		return
	}

	okResponse(w, videoView)
}

type ListVideoViewRequest struct {
	PageNo 		  int       `json:"page_no" required:"true"`
	PageSize 	  int       `json:"page_size" required:"true"`
	OrderBy 	  string 	`json:"order_by,omitempty"`
	OrderByType	  string 	`json:"order_by_type,omitempty"`
	DateFrom 	  string 	`json:"date_from,omitempty"`
	DateTo 		  string 	`json:"date_to,omitempty"`
}

type ListVideoViewResponse struct {
	VideoViewCount []model.VideoViewCount `json:"video_view_count"`
}

func ListVideoView(w http.ResponseWriter, r *http.Request) {
	var req ListVideoViewRequest
	var err error

	req.PageNo, err = parseIntParam(r, "page_no")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseQuery)
		return
	}
	
	req.PageSize, err = parseIntParam(r, "page_size")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseQuery)
		return
	}

	req.OrderBy = parseStringParam(r, "order_by")
	req.OrderByType = parseStringParam(r, "order_by_type")
	req.DateFrom = parseStringParam(r, "date_from")
	req.DateTo = parseStringParam(r, "date_to")

	query := "SELECT video_id, COUNT(*) AS total_view_count FROM video_view"
	args := []interface{}{}

	now := time.Now()
	if req.DateTo == "" {
		req.DateTo = now.Format("2006-01-02")
	}

	if req.DateFrom == "" {
		req.DateFrom = now.AddDate(0, 0, -7).Format("2006-01-02")
	}

	query += " WHERE viewed_at BETWEEN ? AND ?"
	args = append(args, req.DateFrom, req.DateTo)

	query += "GROUP BY video_id"
	
	if req.OrderBy == "" {
		req.OrderBy = "total_view_count"
	}

	if req.OrderByType == "" {
		req.OrderByType = "DESC"
	}

	query += " ORDER BY " + req.OrderBy + " " + req.OrderByType

	query += " LIMIT ? OFFSET ?"
	args = append(args, req.PageSize, (req.PageNo-1)*req.PageSize)

	videoViewCount, err := model.ListVideoView(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCountVideoView + err.Error())
		return
	}

	res := ListVideoViewResponse{
		VideoViewCount: videoViewCount,
	}

	okResponse(w, res)
}

type ListVideoViewHistoryRequest struct {
	PageNo 		  int       `json:"page_no" required:"true"`
	PageSize 	  int       `json:"page_size" required:"true"`
}

type VideoHistoryResponse struct {
	VideoID           int            `json:"video_id"`
	VideoTitle        string         `json:"video_title"`
	VideoDesc         string         `json:"video_desc"`
	VideoFileID       string         `json:"video_file_id"`
	VideoViewCount    int            `json:"video_view_count"`
	VideoThumbnail    string         `json:"video_thumbnail"`
	VideoInterestID   string         `json:"video_interest_id"`
	VideoInterestInfo map[int]string `json:"video_interest_info"`
	DatePosted        time.Time      `json:"date_posted"`
	ChannelInfo       model.Channel  `json:"channel_info"`
	ViewedAt 		  time.Time      `json:"viewed_at"`
}

type ListVideoViewHistoryResponse struct {
	VideoHistory []VideoHistoryResponse `json:"video_history"`
}

func ListVideoViewHistory(w http.ResponseWriter, r *http.Request) {
	var req ListVideoViewHistoryRequest
	var err error

	vars := mux.Vars(r)
	viewerID := vars["ViewerID"]

	req.PageNo, err = parseIntParam(r, "page_no")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseQuery)
		return
	}
	
	req.PageSize, err = parseIntParam(r, "page_size")
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseQuery)
		return
	}
	
	query := "SELECT video_id, viewer_id, viewed_at FROM video_view WHERE viewer_id = ?"
	args := []interface{}{viewerID}

	query += "ORDER BY viewed_at DESC LIMIT ? OFFSET ?"
	args = append(args, req.PageSize, (req.PageNo-1)*req.PageSize)

	videoView, err := model.ListVideoViewHistory(query, args...)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCountVideoView + err.Error())
		return
	}

	videoHistory := []VideoHistoryResponse{}

	interest, err := model.ListAllInterest()
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrGetInterest)
		return
	}

	interestMap := makeInterestMap(interest)

	for _, v := range videoView {
		video, err := model.DetailVideo(v.VideoID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrGetDetailVideos + err.Error())
			return
		}

		channel, err := model.DetailChannel(video.ChannelID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrGetDetailChannel + err.Error())
			return
		}

		videoInterestInfo := make(map[int]string)
		for _, interestID := range stringToIntSlice(postgresArrayToString(video.VideoInterestID)) {
			videoInterestInfo[interestID] = interestMap[interestID]
		}

		viewCount, err := model.CountVideoView(video.VideoID)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, ErrCountVideoView)
			return
		}

		videoHistory = append(videoHistory, VideoHistoryResponse{
			VideoID:           video.VideoID,
			VideoTitle:        video.VideoTitle,
			VideoDesc:         video.VideoDesc,
			VideoFileID:       video.VideoFileID,
			VideoViewCount:    viewCount,
			VideoThumbnail:    video.VideoThumbnail,
			VideoInterestID:   video.VideoInterestID,
			VideoInterestInfo: videoInterestInfo,
			DatePosted:        video.DatePosted,
			ChannelInfo:       *channel,
			ViewedAt:          v.ViewedAt,
		})
	}

	res := ListVideoViewHistoryResponse{
		VideoHistory: videoHistory,
	}

	okResponse(w, res)
}

type CountVideoViewResponse struct {
	VideoID int `json:"video_id"`
	TotalViewCount int `json:"total_view_count"`
}

func CountVideoView(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	videoID, err := strconv.Atoi(vars["VideoID"])
	if err != nil {
		errorResponse(w, http.StatusBadRequest, ErrParseBody)
		return
	}

	count, err := model.CountVideoView(videoID)
	if err != nil {
		errorResponse(w, http.StatusInternalServerError, ErrCountVideoView + err.Error())
		return
	}

	res := CountVideoViewResponse{
		VideoID: videoID,
		TotalViewCount: count,
	}

	okResponse(w, res)
}
