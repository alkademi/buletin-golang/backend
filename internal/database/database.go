package database

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	DB *gorm.DB
)

func ConnectDB() error {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	h := os.Getenv("POSTGRES_HOST")
	u := os.Getenv("POSTGRES_USER")
	pwd := os.Getenv("POSTGRES_PASSWORD")
	p := os.Getenv("POSTGRES_PORT")
	d := os.Getenv("POSTGRES_DB")
	dsn := "host=" + h + " user=" + u + " password=" + pwd + " dbname=" + d + " port=" + p + " sslmode=disable TimeZone=Asia/Shanghai"
	dbConnection, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	log.Println(dsn)
	if err != nil {
		return err
	}
	DB = dbConnection
	return nil
}
