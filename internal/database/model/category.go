package model

import (
	"errors"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
)

type Category struct {
	CategoryID      int    `json:"category_id"`
	CategoryName    string `json:"category_name"`
	CategoryPicture string `json:"category_picture"`
}

func (p *Category) TableName() string {
	return "category"
}

func ListCategory() ([]Category, error) {
	db := database.DB

	var categories []Category

	err := db.Debug().Table("category").Model(&Category{}).Find(&categories).Error
	if err != nil {
		return nil, err
	}
	return categories, nil
}

func CreateCategory(categoryName, categoryPicture string) (*Category, error) {
	db := database.DB

	var categoryID int

	tx := db.Debug().Raw("INSERT INTO category (category_name, category_picture) VALUES ($1, $2) RETURNING category_id", categoryName, categoryPicture).Scan(&categoryID)
	if tx.Error != nil {
		return nil, tx.Error
	}

	category := Category{
		CategoryID:      categoryID,
		CategoryName:    categoryName,
		CategoryPicture: categoryPicture,
	}

	return &category, nil
}

func UpdateCategory(categoryID int, categoryName, categoryPicture string) (*Category, error) {
	db := database.DB

	category := Category{}

	tx := db.Debug().Table("category").Model(&Category{}).Where("category_id = ?", categoryID).Take(&Category{}).UpdateColumns(
		map[string]interface{}{
			"category_name":    categoryName,
			"category_picture": categoryPicture,
		},
	).Scan(&category)
	if tx.Error != nil {
		return nil, tx.Error
	}

	if tx.RowsAffected == 0 {
		return nil, errors.New("Channel not found")
	}

	return &category, nil
}

func DeleteCategory(categoryID int) (int, error) {
	db := database.DB

	tx := db.Debug().Table("category").Model(&Category{}).Where("category_id = ?", categoryID).Take(&Category{}).Delete(&Category{})
	if tx.Error != nil {
		return 0, tx.Error
	}
	return int(tx.RowsAffected), nil
}

func DetailCategory(categoryID int) (*Category, error) {
	db := database.DB
	category := Category{}
	err := db.Debug().Table("category").Model(&Category{}).Where("category_id = ?", categoryID).Find(&category).Error
	if err != nil {
		return nil, err
	}
	return &category, nil
}
