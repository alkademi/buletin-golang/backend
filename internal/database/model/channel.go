package model

import (
	"errors"
	"time"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
)

type Channel struct {
	ChannelID      int    `json:"channel_id"`
	OwnerID 		int    `json:"owner_id"`
	ChannelName    string    `json:"channel_name"`
	ChannelPicture string    `json:"channel_picture"`
	CreatedAt      time.Time `json:"created_at"`
}

func ListChannel(query string, args ...interface{}) ([]Channel, error) {
	db := database.DB

	var channels []Channel
	var err error

	if len(args) > 0 {
		err = db.Raw(query, args...).Scan(&channels).Error
	} else {
		err = db.Raw(query).Scan(&channels).Error
	}	

	if err != nil {
		return nil, err
	}

	return channels, nil
}

func DetailChannel(channelID int) (*Channel, error) {
	db := database.DB
	channel := Channel{}
	err := db.Debug().Table("channel").Model(&Channel{}).Where("channel_id = ?", channelID).Find(&channel).Error
	if err != nil {
		return nil, err
	}
	return &channel, nil
}

func CreateChannel(ownerID int, channelName string, channelPicture string) (*Channel, error) {
	db := database.DB

	var channelID int

	tx := db.Debug().Raw("INSERT INTO channel (owner_id, channel_name, channel_picture) VALUES ($1, $2, $3) RETURNING channel_id", ownerID, channelName, channelPicture).Scan(&channelID)
	if tx.Error != nil {
		return nil, tx.Error
	}

	channel := Channel{
		ChannelID:      channelID,
		OwnerID: 		ownerID,
		ChannelName:    channelName,
		ChannelPicture: channelPicture,
		CreatedAt:      time.Now(),
	}

	return &channel, nil
}

func UpdateChannel(channelID int,  channelName string, channelPicture string) (*Channel, error) {
	db := database.DB

	channel := Channel{}

	tx := db.Debug().Table("channel").Model(&Channel{}).Where("channel_id = ?", channelID).Take(&Channel{}).UpdateColumns(
		map[string]interface{}{
			"channel_name":    channelName,
			"channel_picture": channelPicture,
		},
	).Scan(&channel)
	if tx.Error != nil {
		return nil, tx.Error
	}
	
	if tx.RowsAffected == 0 {
		return nil, errors.New("Channel not found")
	}

	return &channel, nil
}

func DeleteChannel(channelID int) (int, error) {
	db := database.DB

	tx := db.Debug().Table("channel").Model(&Channel{}).Where("channel_id = ?", channelID).Take(&Channel{}).Delete(&Channel{})
	if tx.Error != nil {
		return 0, tx.Error
	}
	return int(tx.RowsAffected), nil
}