package model

import (
	"errors"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
)

type Interest struct {
	InterestID   int    `json:"interest_id"`
	InterestName string `json:"interest_name"`
}

func ListInterest(query string, args ...interface{}) ([]Interest, error) {
	db := database.DB

	var interests []Interest
	var err error

	if len(args) > 0 {
		err = db.Raw(query, args...).Scan(&interests).Error
	} else {
		err = db.Raw(query).Scan(&interests).Error
	}

	if err != nil {
		return nil, err
	}

	return interests, nil
}

func ListAllInterest() ([]Interest, error) {
	query := "SELECT * FROM interest"
	args := []interface{}{}

	return ListInterest(query, args...)
}

func ListInterestByID(interestID []int) ([]Interest, error) {
	query := "SELECT * FROM interest WHERE interest_id IN (?)"
	args := []interface{}{interestID}

	return ListInterest(query, args...)
}

func GetInterest(interestID int) (*Interest, error) {
	db := database.DB

	interest := Interest{}

	err := db.Debug().Table("interest").Model(&Interest{}).Where("interest_id = ?", interestID).Take(&Interest{}).Scan(&interest).Error
	if err != nil {
		return nil, err
	}

	return &interest, nil
}

func CreateInterest(interestName string) (*Interest, error) {
	db := database.DB

	var interestID int

	tx := db.Debug().Raw("INSERT INTO interest (interest_name) VALUES ($1) RETURNING interest_id", interestName).Scan(&interestID)
	if tx.Error != nil {
		return nil, tx.Error
	}

	interest := Interest{
		InterestID:   interestID,
		InterestName: interestName,
	}

	return &interest, nil
}

func UpdateInterest(interestID int, interestName string) (*Interest, error) {
	db := database.DB

	interest := Interest{}

	tx := db.Debug().Table("interest").Model(&Interest{}).Where("interest_id = ?", interestID).Take(&Interest{}).UpdateColumns(
		map[string]interface{}{
			"interest_name": interestName,
		},
	).Scan(&interest)
	if tx.Error != nil {
		return nil, tx.Error
	}
	
	if tx.RowsAffected == 0 {
		return nil, errors.New("Interest not found")
	}

	return &interest, nil
}

func DeleteInterest(interestID int) (int, error) {
	db := database.DB

	tx := db.Debug().Table("interest").Model(&Channel{}).Where("interest_id = ?", interestID).Take(&Interest{}).Delete(&Interest{})
	if tx.Error != nil {
		return 0, tx.Error
	}
	return int(tx.RowsAffected), nil
}