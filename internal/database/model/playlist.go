package model

import (
	"errors"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
)

type Playlist struct {
	PlaylistID 	int    `json:"playlist_id"`
	PlaylistName string `json:"playlist_name"`
	PlaylistPicture string `json:"playlist_picture"`
	CategoryID 	int `json:"category_id"`
}

func ListPlaylist(query string, args ...interface{}) ([]Playlist, error) {
	db := database.DB

	var playlists []Playlist
	var err error

	if len(args) > 0 {
		err = db.Raw(query, args...).Scan(&playlists).Error
	} else {
		err = db.Raw(query).Scan(&playlists).Error
	}	

	if err != nil {
		return nil, err
	}

	return playlists, nil
}

func DetailPlaylist(playlistID int) (*Playlist, error) {
	db := database.DB
	playlist := Playlist{}
	err := db.Debug().Table("playlist").Model(&Playlist{}).Where("playlist_id = ?", playlistID).Find(&playlist).Error
	if err != nil {
		return nil, err
	}

	return &playlist, nil
}

func CreatePlaylist(playlistName, playlistPicture string, categoryID int) (*Playlist, error) {
	db := database.DB
	
	var PlaylistID int
	
	tx := db.Debug().Raw("INSERT INTO playlist (playlist_name, playlist_picture, category_id) VALUES (?, ?, ?) RETURNING playlist_id", playlistName, playlistPicture, categoryID).Scan(&PlaylistID)
	if tx.Error != nil {
		return nil, tx.Error
	}

	playlist := Playlist{
		PlaylistID: PlaylistID,
		PlaylistName: playlistName,
		PlaylistPicture: playlistPicture,
		CategoryID: categoryID,
	}

	return &playlist, nil
}

func UpdatePlaylist(playlistID int, playlistName, playlistPicture string, categoryID int) (*Playlist, error) {
	db := database.DB

	var playlist Playlist

	tx := db.Debug().Table("playlist").Model(&Playlist{}).Where("playlist_id = ?", playlistID).Take(&Playlist{}).UpdateColumns(
		map[string]interface{}{
			"playlist_name": playlistName,
			"playlist_picture": playlistPicture,
			"category_id": categoryID,
		},
	).Scan(&playlist)
	if tx.Error != nil {
		return nil, tx.Error
	}
	
	if tx.RowsAffected == 0 {
		return nil, errors.New("playlist not found")
	}

	return &playlist, nil
}

func DeletePlaylist(playlistID int) (int, error) {
	db := database.DB

	tx := db.Debug().Table("playlist").Model(&Playlist{}).Where("playlist_id = ?", playlistID).Take(&Playlist{}).Delete(&Playlist{})
	if tx.Error != nil {
		return 0, tx.Error
	}

	if tx.RowsAffected == 0 {
		return 0, errors.New("Playlist not found")
	}

	return int(tx.RowsAffected), nil
}

type VideoPlaylist struct {
	VideoID int `json:"video_id"`
	PlaylistID int `json:"playlist_id"`
}

func AddVideoToPlaylist(playlistID int, videoID int) (int, error) {
	db := database.DB

	tx := db.Exec("INSERT INTO video_playlist (video_id, playlist_id) VALUES (?, ?)", videoID, playlistID)
	
	return int(tx.RowsAffected), nil
}

func DeleteVideoFromPlaylist(playlistID int, videoID int) (int, error) {
	db := database.DB

	tx := db.Debug().Table("video_playlist").Model(&VideoPlaylist{}).Where("video_id = ? AND playlist_id = ?", videoID, playlistID).Take(&VideoPlaylist{}).Delete(&VideoPlaylist{})
	if tx.Error != nil {
		return 0, tx.Error
	}

	if tx.RowsAffected == 0 {
		return 0, errors.New("Video not found")
	}

	return int(tx.RowsAffected), nil
}