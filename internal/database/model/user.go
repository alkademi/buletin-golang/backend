package model

import (
	"errors"
	"time"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
)

type User struct {
	AccountID             int       `json:"account_id"`
	AccountEmail          string    `json:"account_email"`
	AccountHashedPassword string    `json:"account_hashed_password"`
	AccountFullname       string    `json:"account_fullname"`
	AccountPhoneNumber    string    `json:"account_phone_number"`
	AccountInterestID     string    `json:"account_interest_id"`
	Role                  string    `json:"role"`
	CreatedAt             time.Time `json:"created_at"`
}

func (p *User) TableName() string {
	return "account"
}

func GetUserByID(userID int) (*User, error) {
	db := database.DB
	user := User{}
	err := db.Debug().Table("account").Model(&User{}).Where("account_id = ?", userID).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func GetUserByCredentials(email string, password string) (*User, error) {
	db := database.DB
	user := User{}
	err := db.Debug().Table("account").Model(&User{}).Where("account_email = ? AND account_hashed_password = ?", email, password).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func GetUserByEmail(email string) (*User, error) {
	db := database.DB
	user := User{}
	err := db.Debug().Table("account").Model(&User{}).Where("account_email = ?", email).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func CreateUser(email string, password string, fullname string, phoneNumber string, interestID string, role string) (*User, error) {
	db := database.DB
	var accID int

	tx := db.Raw("INSERT INTO account (account_email, account_hashed_password, account_fullname, account_phone_number, account_interest_id, role) VALUES ($1, $2, $3, $4, $5, $6) RETURNING account_id", email, password, fullname, phoneNumber, interestID, role).Scan(&accID)
	if tx.Error != nil {
		return nil, tx.Error
	}

	user := User{
		AccountID:             accID,
		AccountHashedPassword: password,
		AccountEmail:          email,
		AccountFullname:       fullname,
		AccountPhoneNumber:    phoneNumber,
		AccountInterestID:     interestID,
		Role:                  role,
		CreatedAt:             time.Now(),
	}

	return &user, nil
}

func ChangePasswordUser(email string, password string) (*User, error) {
	db := database.DB

	user := User{}

	tx := db.Debug().Table("account").Model(&Category{}).Where("account_email = ?", email).Take(&User{}).UpdateColumns(
		map[string]interface{}{
			"account_hashed_password": password,
		},
	).Scan(&user)

	if tx.Error != nil {
		return nil, tx.Error
	}

	if tx.RowsAffected == 0 {
		return nil, errors.New("User not found")
	}

	return &user, nil
}
