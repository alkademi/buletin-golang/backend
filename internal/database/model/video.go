package model

import (
	"time"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
)

type Video struct {
	VideoID         int       `json:"video_id"`
	ChannelID       int       `json:"channel_id"`
	VideoTitle      string    `json:"video_title"`
	VideoDesc       string    `json:"video_desc"`
	VideoFileID     string    `json:"video_file_id"`
	VideoThumbnail  string    `json:"video_thumbnail"`
	VideoInterestID string    `json:"video_interest_id"`
	DatePosted      time.Time `json:"date_posted"`
}

func ListVideos(query string, args ...interface{}) ([]Video, error) {
	var videos []Video
	var err error
	db := database.DB

	if len(args) > 0 {
		err = db.Raw(query, args...).Scan(&videos).Error
	} else {
		err = db.Raw(query).Scan(&videos).Error
	}

	if err != nil {
		return nil, err
	}
	return videos, nil
}

func DetailVideo(videoID int) (*Video, error) {
	db := database.DB
	video := Video{}
	err := db.Debug().Table("video").Model(&Video{}).Where("video_id = ?", videoID).Find(&video).Error
	if err != nil {
		return nil, err
	}
	return &video, nil
}

func CreateVideo(channelID int, title string, desc string, fileID string, thumbnail string, interestID string) (*Video, error) {
	db := database.DB

	var videoID int

	tx := db.Raw("INSERT INTO video (channel_id, video_title, video_desc, video_file_id, video_thumbnail, video_interest_id) VALUES ($1, $2, $3, $4, $5, $6) RETURNING video_id", channelID, title, desc, fileID, thumbnail, interestID).Scan(&videoID)
	if tx.Error != nil {
		return nil, tx.Error
	}

	video := Video{
		VideoID:         videoID,
		ChannelID:       channelID,
		VideoTitle:      title,
		VideoDesc:       desc,
		VideoFileID:     fileID,
		VideoThumbnail:  thumbnail,
		VideoInterestID: interestID,
	}
	return &video, nil
}

func UpdateVideo(videoID int, title string, desc string, fileID string, thumbnail string, interest_id string) (*Video, error) {
	db := database.DB

	video := &Video{}

	tx := db.Debug().Table("video").Model(video).Where("video_id = ?", videoID).UpdateColumns(
		map[string]interface{}{
			"video_title":       title,
			"video_desc":        desc,
			"video_file_id":     fileID,
			"video_thumbnail":   thumbnail,
			"video_interest_id": interest_id,
		},
	).Scan(&video)
	if tx.Error != nil {
		return nil, tx.Error
	}
	if tx.RowsAffected == 0 {
		return nil, nil
	}

	return video, nil
}

func DeleteVideo(videoID int) (int, error) {
	db := database.DB

	tx := db.Debug().Table("video").Model(&Video{}).Where("video_id = ?", videoID).Delete(&Video{})
	if tx.Error != nil {
		return 0, tx.Error
	}
	return int(tx.RowsAffected), nil
}
