package model

import (
	"time"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database"
	"gorm.io/gorm/clause"
)

type VideoView struct {
	VideoID  int       `json:"video_id"`
	ViewerID string    `json:"viewer_id"`
	ViewedAt time.Time `json:"viewed_at"`
}

func (p *VideoView) TableName() string {
	return "video_view"
}

func CreateVideoView(videoID int, viewerID string) (*VideoView, error) {
	db := database.DB

	videoView := &VideoView{
		VideoID:  videoID,
		ViewerID: viewerID,
		ViewedAt: time.Now(),
	}

	tx := db.Debug().Table("video_view").Create(videoView)
	if tx.Error != nil {
		return nil, tx.Error
	}

	return videoView, nil
}

func UpsertVideoView(videoID int, viewerID string) (*VideoView, error) {
	db := database.DB

	videoView := &VideoView{
		VideoID:  videoID,
		ViewerID: viewerID,
		ViewedAt: time.Now(),
	}

	tx := db.Debug().Table("video_view").Clauses(
		clause.OnConflict{
			Columns:   []clause.Column{{Name: "video_id"}, {Name: "viewer_id"}},
			DoUpdates: clause.AssignmentColumns([]string{"viewed_at"}),
		}).Create(&videoView)
	if tx.Error != nil {
		return nil, tx.Error
	}

	return videoView, nil
}

type VideoViewCount struct {
	VideoID        int `json:"video_id"`
	TotalViewCount int `json:"total_view_count"`
}

func CountVideoView(videoID int) (int, error) {
	db := database.DB

	var count int

	tx := db.Raw("SELECT COUNT(*) FROM video_view WHERE video_id = $1", videoID).Scan(&count)
	if tx.Error != nil {
		return 0, tx.Error
	}

	return count, nil
}

func ListVideoView(query string, args ...interface{}) ([]VideoViewCount, error) {
	db := database.DB

	var videoViewCounts []VideoViewCount

	tx := db.Raw(query, args...).Scan(&videoViewCounts)
	if tx.Error != nil {
		return nil, tx.Error
	}

	return videoViewCounts, nil
}

func ListVideoViewHistory(query string, args ...interface{}) ([]VideoView, error) {
	db := database.DB

	var videoViews []VideoView

	tx := db.Raw(query, args...).Scan(&videoViews)
	if tx.Error != nil {
		return nil, tx.Error
	}

	return videoViews, nil
}
