package middleware

import (
	"encoding/json"
	"net/http"
)

func sendResponse(w http.ResponseWriter, status int, data interface{}) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func errorResponse(w http.ResponseWriter, status int, err string) {
	sendResponse(w, status, map[string]string{
		"error": err,
	})
}
