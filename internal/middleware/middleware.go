package middleware

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

var JWT_SIGNATURE_KEY = []byte(os.Getenv("JWT_SIGNATURE_KEY"))

func IsAuthorizedUser(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			errorResponse(w, http.StatusBadRequest, "Invalid Token")
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("error in parsing")
			} else if method != jwt.SigningMethodHS256 {
				return nil, errors.New("invalid signing method")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			errorResponse(w, http.StatusUnauthorized, err.Error())
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			accID := fmt.Sprintf("%v", claims["account_id"])
			r.Header.Set("AccountID", accID)
			
			currentTime := time.Now()
			expireTime := time.Unix(int64(claims["exp"].(float64)), 0)
			if claims["role"] == "user" {
				r.Header.Set("Role", "user")
				next.ServeHTTP(w, r)
			} else if currentTime.After(expireTime) {
				errorResponse(w, http.StatusUnauthorized, "Token Expired")
			} else {
				errorResponse(w, http.StatusUnauthorized, "Invalid Role")
				return
			}
		}
	})
}

func IsAuthAdmin(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			errorResponse(w, http.StatusBadRequest, "Invalid Token")
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("error in parsing")
			} else if method != jwt.SigningMethodHS256 {
				return nil, errors.New("invalid signing method")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			errorResponse(w, http.StatusUnauthorized, err.Error())
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			accID := fmt.Sprintf("%v", claims["account_id"])
			r.Header.Set("AccountID", accID)

			currentTime := time.Now()
			expireTime := time.Unix(int64(claims["exp"].(float64)), 0)
			if claims["role"] == "admin" {
				r.Header.Set("Role", "admin")
				next.ServeHTTP(w, r)
			} else if currentTime.After(expireTime) {
				errorResponse(w, http.StatusUnauthorized, "Token Expired")
			} else {
				errorResponse(w, http.StatusUnauthorized, "Invalid Role")
				return
			}
		}
	})
}

func IsAuthSuperAdmin(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			errorResponse(w, http.StatusBadRequest, "Invalid Token")
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("error in parsing")
			} else if method != jwt.SigningMethodHS256 {
				return nil, errors.New("invalid signing method")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			errorResponse(w, http.StatusUnauthorized, err.Error())
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			accID := fmt.Sprintf("%v", claims["account_id"])
			r.Header.Set("AccountID", accID)

			currentTime := time.Now()
			expireTime := time.Unix(int64(claims["exp"].(float64)), 0)
			if claims["role"] == "superadmin" {
				r.Header.Set("Role", "superadmin")
				next.ServeHTTP(w, r)
			} else if currentTime.After(expireTime) {
				errorResponse(w, http.StatusUnauthorized, "Token Expired")
			} else {
				errorResponse(w, http.StatusUnauthorized, "Invalid Role")
				return
			}
		}
	})
}

func IsAuthAdminOrSuperAdmin(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			errorResponse(w, http.StatusBadRequest, "Invalid Token")
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("error in parsing")
			} else if method != jwt.SigningMethodHS256 {
				return nil, errors.New("invalid signing method")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			errorResponse(w, http.StatusUnauthorized, err.Error())
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			accID := fmt.Sprintf("%v", claims["account_id"])
			r.Header.Set("AccountID", accID)
			
			currentTime := time.Now()
			expireTime := time.Unix(int64(claims["exp"].(float64)), 0)
			if claims["role"] == "admin" || claims["role"] == "superadmin" {
				r.Header.Set("Role", "admin")
				next.ServeHTTP(w, r)
			} else if currentTime.After(expireTime) {
				errorResponse(w, http.StatusUnauthorized, "Token Expired")
			} else {
				errorResponse(w, http.StatusUnauthorized, "Invalid Role")
				return
			}
		}
	})
}

func GetAccountID(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")
		if !strings.Contains(authorizationHeader, "Bearer") {
			errorResponse(w, http.StatusBadRequest, "Invalid Token")
			return
		}

		tokenString := strings.Replace(authorizationHeader, "Bearer ", "", -1)

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("error in parsing")
			} else if method != jwt.SigningMethodHS256 {
				return nil, errors.New("invalid signing method")
			}

			return JWT_SIGNATURE_KEY, nil
		})
		if err != nil {
			errorResponse(w, http.StatusUnauthorized, err.Error())
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			accID := fmt.Sprintf("%v", claims["account_id"])
			r.Header.Set("AccountID", accID)
			next.ServeHTTP(w, r)
		}
	})
}
