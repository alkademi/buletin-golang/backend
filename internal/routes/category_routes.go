package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/middleware"
)

func CategoryRoutesGroup(r *mux.Router) {
	r.HandleFunc("/category", controllers.ListCategory).Methods("GET")
	r.HandleFunc("/category", middleware.IsAuthSuperAdmin(controllers.CreateCategory)).Methods("POST")
	r.HandleFunc("/category/{CategoryID}", middleware.IsAuthSuperAdmin(controllers.UpdateCategory)).Methods("PUT")
	r.HandleFunc("/category/{CategoryID}", middleware.IsAuthSuperAdmin(controllers.DeleteCategory)).Methods("DELETE")
	r.HandleFunc("/category/{CategoryID}", controllers.GetCategory).Methods("GET")
}
