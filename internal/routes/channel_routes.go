package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/middleware"
)

func ChannelRoutesGroup(r *mux.Router) {
	r.HandleFunc("/channel", controllers.ListChannel).Methods("GET")
	r.HandleFunc("/channel", middleware.IsAuthAdminOrSuperAdmin(controllers.CreateChannel)).Methods("POST")
	r.HandleFunc("/channel/{ChannelID}", middleware.IsAuthAdminOrSuperAdmin(controllers.UpdateChannel)).Methods("PUT")
	r.HandleFunc("/channel/{ChannelID}", middleware.IsAuthAdminOrSuperAdmin(controllers.DeleteChannel)).Methods("DELETE")
	r.HandleFunc("/channel/{ChannelID}", controllers.GetChannel).Methods("GET")
}
