package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/middleware"
)

func InterestRoutesGroup(r *mux.Router) {
	r.HandleFunc("/interest", controllers.ListInterest).Methods("GET")
	r.HandleFunc("/interest", middleware.IsAuthSuperAdmin(controllers.CreateInterest)).Methods("POST")
	r.HandleFunc("/interest/{InterestID}", controllers.GetInterest).Methods("GET")
	r.HandleFunc("/interest/{InterestID}", middleware.IsAuthSuperAdmin(controllers.UpdateInterest)).Methods("PUT")
	r.HandleFunc("/interest/{InterestID}", middleware.IsAuthSuperAdmin(controllers.DeleteInterest)).Methods("DELETE")
}
