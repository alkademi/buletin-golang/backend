package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/middleware"
)

func PlaylistRoutesGroup(r *mux.Router) {
	r.HandleFunc("/playlist", controllers.ListPlaylist).Methods("GET")
	r.HandleFunc("/playlist", middleware.IsAuthAdminOrSuperAdmin(controllers.CreatePlaylist)).Methods("POST")
	r.HandleFunc("/playlist/{PlaylistID}/add-video", middleware.IsAuthAdminOrSuperAdmin(controllers.AddVideoToPlaylist)).Methods("POST")
	r.HandleFunc("/playlist/{PlaylistID}/delete-video", middleware.IsAuthAdminOrSuperAdmin(controllers.DeleteVideoFromPlaylist)).Methods("DELETE")
	r.HandleFunc("/playlist/{PlaylistID}", middleware.IsAuthAdminOrSuperAdmin(controllers.UpdatePlaylist)).Methods("PUT")
	r.HandleFunc("/playlist/{PlaylistID}", middleware.IsAuthAdminOrSuperAdmin(controllers.DeletePlaylist)).Methods("DELETE")
	r.HandleFunc("/playlist/{PlaylistID}", controllers.GetPlaylist).Methods("GET")
}
