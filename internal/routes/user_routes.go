package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/middleware"
)

func UserRoutesGroup(r *mux.Router) {
	r.HandleFunc("/login", controllers.Login).Methods("POST")
	r.HandleFunc("/user", controllers.Register).Methods("POST")
	r.HandleFunc("/user", controllers.ChangePassword).Methods("PUT")
	r.HandleFunc("/user/reset", controllers.ResetPassword).Methods("PUT")
	r.HandleFunc("/user/reset/check", controllers.CheckTokenResetPassword).Methods("POST")
	r.HandleFunc("/forgot", controllers.ForgotPassword).Methods("POST")
	r.HandleFunc("/profile", middleware.GetAccountID(controllers.GetProfile)).Methods("GET")
}
