package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/middleware"
)

func VideoRoutesGroup(r *mux.Router) {
	r.HandleFunc("/video", controllers.ListVideos).Methods("GET")
	r.HandleFunc("/video/hot", controllers.ListHotVideos).Methods("GET")
	r.HandleFunc("/video", middleware.IsAuthAdminOrSuperAdmin(controllers.CreateVideo)).Methods("POST")
	r.HandleFunc("/video/{VideoID}", middleware.IsAuthAdminOrSuperAdmin(controllers.UpdateVideo)).Methods("PUT")
	r.HandleFunc("/video/{VideoID}", middleware.IsAuthAdminOrSuperAdmin(controllers.DeleteVideo)).Methods("DELETE")
	r.HandleFunc("/video/{VideoID}", controllers.DetailVideo).Methods("GET")
}
