package routes

import (
	"github.com/gorilla/mux"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/controllers"
)

func VideoViewRoutesGroup(r *mux.Router) {
	r.HandleFunc("/video-view", controllers.ListVideoView).Methods("GET")
	r.HandleFunc("/video-view", controllers.CreateVideoView).Methods("POST")
	r.HandleFunc("/video-view/{VideoID}", controllers.CountVideoView).Methods("GET")
	r.HandleFunc("/history/{ViewerID}", controllers.ListVideoViewHistory).Methods("GET")
}
