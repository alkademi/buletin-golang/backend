package util

import "time"

func ValidateDate(date string) error {
	_, err := time.Parse("2006-01-02", date)
	if err != nil {
		return err
	}
	return nil
}