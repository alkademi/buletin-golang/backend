package util

import (
	"fmt"
	"log"
	"net/smtp"
	"strings"
	"time"

	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

var (
	EMAIL_SMTP_HOST     = "smtp.gmail.com"
	EMAIL_SMTP_PORT     = 587
	EMAIL_SENDER_NAME   = "Buletin.id <buletin16.k2@gmail.com>"
	EMAIL_AUTH_EMAIL    = "buletin16.k2@gmail.com"
	EMAIL_AUTH_PASSWORD = "RahasiaBulet1n"
)

func SendMail(to []string, subject, message string) {
	headers := make(map[string]string)
	headers["Subject"] = subject
	headers["Content-Type"] = "text/html; charset=\"utf-8\""
	headers["Content-Transfer-Encoding"] = "base64"

	body := ""

	for k, v := range headers {
		body += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	body += "From: " + EMAIL_SENDER_NAME + "\n" +
		"To: " + strings.Join(to, ",") + "\n" +
		"Subject: " + subject + "\n\n" +
		message

	auth := smtp.PlainAuth("", EMAIL_AUTH_EMAIL, EMAIL_AUTH_PASSWORD, EMAIL_SMTP_HOST)
	smtpAddr := fmt.Sprintf("%s:%d", EMAIL_SMTP_HOST, EMAIL_SMTP_PORT)

	err := smtp.SendMail(smtpAddr, auth, EMAIL_AUTH_EMAIL, to, []byte(body))
	if err != nil {
		log.Println(err)
	}

	log.Println("Mail successfully sent to:", to)
}

func SendNewAdminAccountEmail(user *model.User) {
	createdAtString := user.CreatedAt.Format("2006-01-02 15:04:05")

	to := []string{user.AccountEmail}
	subject := "Registrasi akun " + user.Role + " baru anda telah berhasil!"
	message := fmt.Sprintf(`
	<html>
		<body>
			<span style="color: #000;">
				<span style="opacity: 0">[%v]</span><br/>
				Halo %s,<br/><br/>
		
				Akun %v anda telah berhasil dibuat pada %v. <br/><br/>
				Berikut adalah kredensial akun anda: <br/> 
				<strong>Nama</strong>: %v <br/> 
				<strong>Password</strong>: %v <br/><br/>

				Silakan masuk melalui pranala berikut http://34.101.91.215:3000/login <br/><br/> 
		
				Terima kasih <br/><br/>

				<span style="opacity: 0">[%v]</span>
			</span>
		</body>
	</html>`, createdAtString, user.AccountFullname, user.Role, createdAtString, user.AccountEmail, user.AccountHashedPassword, createdAtString)

	go SendMail(to, subject, message)
}

func SendForgotPasswordEmail(email, fullName, url string) {
	createdAtString := time.Now()
	to := []string{email}
	subject := "Penggantian password untuk akun dengan email " + email
	message := fmt.Sprintf(`
	<html>
		<body>
			<span style="color: #000;">
				<span style="opacity: 0">[%v]</span><br/>
				Halo %s,<br/><br/>
		
				Silahkan akses pranala berikut untuk mengganti password Anda<br/><br/>
				<a href="%s">%s</a><br/><br/>
		
				Terima kasih <br/><br/>

				<span style="opacity: 0">[%v]</span>
			</span>
		</body>
	</html>`, createdAtString, fullName, url, url, createdAtString)

	go SendMail(to, subject, message)
}
