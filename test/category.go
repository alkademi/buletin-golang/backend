package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

type CategoryRepository interface {
	Get(id int) (*model.Category, error)
	Create(id int, name, picture string) error
}

type repoCategory struct {
	DB *gorm.DB
}

func (p *repoCategory) Create(id int, name, picture string) error {
	category := &model.Category{
		CategoryID:      id,
		CategoryName:    name,
		CategoryPicture: picture,
	}

	return p.DB.Create(category).Error
}

func (p *repoCategory) Get(id int) (*model.Category, error) {
	category := new(model.Category)

	err := p.DB.Where("category_id = ?", id).Find(category).Error

	return category, err
}

func CreateRepositoryCategory(db *gorm.DB) CategoryRepository {
	return &repoCategory{
		DB: db,
	}
}
