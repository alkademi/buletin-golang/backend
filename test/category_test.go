package repository

import (
	"database/sql"
	"regexp"
	"testing"

	"github.com/go-test/deep"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

type SuiteCategory struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository CategoryRepository
	category   *model.Category
}

func (s *SuiteCategory) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("postgres", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.repository = CreateRepositoryCategory(s.DB)
}

func (s *SuiteCategory) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInitCategory(t *testing.T) {
	suite.Run(t, new(SuiteCategory))
}

func (s *SuiteCategory) Test_repository_Create_Category() {
	var (
		id      = 1
		name    = "test-name"
		picture = "test-name-url"
	)

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(`INSERT INTO "category"`)).
		WithArgs(id, name, picture).
		WillReturnResult(sqlmock.NewResult(0, 1))
	s.mock.ExpectCommit()

	err := s.repository.Create(id, name, picture)

	require.NoError(s.T(), err)
}

func (s *SuiteCategory) Test_repository_Get_Categor() {
	var (
		id      = 1
		name    = "test-name"
		picture = "test-name-url"
	)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "category" WHERE (category_id = $1)`)).
		WithArgs(id).
		WillReturnRows(sqlmock.NewRows([]string{"category_id", "category_name", "category_picture"}).
			AddRow(id, name, picture))

	res, err := s.repository.Get(id)

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(&model.Category{CategoryID: id, CategoryName: name, CategoryPicture: picture}, res))
}
