package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

type UserRepository interface {
	Get(id int) (*model.User, error)
	Create(id int, account_email, account_password, account_fullname, account_phone, account_interest_id, role string, created_at time.Time) error
}

type userCategory struct {
	DB *gorm.DB
}

func (p *userCategory) Create(id int, account_email, account_password, account_fullname, account_phone_number, account_interest_id, role string, created_at time.Time) error {
	user := &model.User{
		AccountID:             id,
		AccountEmail:          account_email,
		AccountHashedPassword: account_password,
		AccountFullname:       account_fullname,
		AccountPhoneNumber:    account_phone_number,
		AccountInterestID:     account_interest_id,
		Role:                  role,
		CreatedAt:             created_at,
	}

	return p.DB.Create(user).Error
}

func (p *userCategory) Get(id int) (*model.User, error) {
	user := new(model.User)

	err := p.DB.Where("account_id = ?", id).Find(user).Error

	return user, err
}

func CreateUserRepository(db *gorm.DB) UserRepository {
	return &userCategory{
		DB: db,
	}
}
