package repository

import (
	"database/sql"
	"regexp"
	"testing"
	"time"

	"github.com/go-test/deep"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

type SuiteUser struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository UserRepository
	user       *model.User
}

func (s *SuiteUser) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("postgres", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.repository = CreateUserRepository(s.DB)
}

func (s *SuiteUser) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInitUser(t *testing.T) {
	suite.Run(t, new(SuiteUser))
}

func (s *SuiteUser) Test_repository_Create_User() {
	var (
		id                      = 1
		account_email           = "test@gmail.com"
		account_hashed_password = "test_pass"
		account_fullname        = "test_user"
		account_phone_number    = "12345678"
		account_interest_id     = "1,2,3"
		role                    = "user"
		created_at              = time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)
	)

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(`INSERT INTO "account"`)).
		WithArgs(id, account_email, account_hashed_password, account_fullname, account_phone_number, account_interest_id, role, created_at).
		WillReturnResult(sqlmock.NewResult(0, 1))
	s.mock.ExpectCommit()

	err := s.repository.Create(id, account_email, account_hashed_password, account_fullname, account_phone_number, account_interest_id, role, created_at)

	require.NoError(s.T(), err)
}

func (s *SuiteUser) Test_repository_Get_User() {
	var (
		id                      = 1
		account_email           = "test@gmail.com"
		account_hashed_password = "test_pass"
		account_fullname        = "test_user"
		account_phone_number    = "12345678"
		account_interest_id     = "1,2,3"
		role                    = "user"
		created_at              = time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)
	)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "account" WHERE (account_id = $1)`)).
		WithArgs(id).
		WillReturnRows(sqlmock.NewRows([]string{"account_id", "account_email", "account_hashed_password", "account_fullname", "account_phone_number", "account_interest_id", "role", "created_at"}).
			AddRow(id, account_email, account_hashed_password, account_fullname, account_phone_number, account_interest_id, role, created_at))

	res, err := s.repository.Get(id)

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(&model.User{AccountID: id, AccountEmail: account_email, AccountHashedPassword: account_hashed_password, AccountFullname: account_fullname, AccountPhoneNumber: account_phone_number, AccountInterestID: account_interest_id, Role: role, CreatedAt: created_at}, res))
}
