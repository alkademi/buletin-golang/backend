package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
)

type VideoViewRepository interface {
	Get(id int) (*model.VideoView, error)
	Create(id int, viewer_id string, viewed_at time.Time) error
}

type repoVideoView struct {
	DB *gorm.DB
}

func (p *repoVideoView) Create(id int, viewer_id string, viewed_at time.Time) error {
	video_view := &model.VideoView{
		VideoID:  id,
		ViewerID: viewer_id,
		ViewedAt: viewed_at,
	}

	return p.DB.Create(video_view).Error
}

func (p *repoVideoView) Get(id int) (*model.VideoView, error) {
	video_view := new(model.VideoView)

	err := p.DB.Where("video_id = ?", id).Find(video_view).Error

	return video_view, err
}

func CreateVideoViewRepository(db *gorm.DB) VideoViewRepository {
	return &repoVideoView{
		DB: db,
	}
}
