package repository

import (
	"database/sql"
	"regexp"
	"testing"
	"time"

	"github.com/go-test/deep"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.informatika.org/buletin_16/if3250_2022_16_buletin_backend/internal/database/model"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

type SuiteVideoView struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository VideoViewRepository
	video_view *model.VideoView
}

func (s *SuiteVideoView) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("postgres", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.repository = CreateVideoViewRepository(s.DB)
}

func (s *SuiteVideoView) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInitVideoView(t *testing.T) {
	suite.Run(t, new(SuiteVideoView))
}

func (s *SuiteVideoView) Test_repository_Create_VideoView() {
	var (
		id        = 1
		viewer_id = "1"
		viewed_at = time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)
	)

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(`INSERT INTO "video_view"`)).
		WithArgs(id, viewer_id, viewed_at).
		WillReturnResult(sqlmock.NewResult(0, 1))
	s.mock.ExpectCommit()

	err := s.repository.Create(id, viewer_id, viewed_at)

	require.NoError(s.T(), err)
}

func (s *SuiteVideoView) Test_repository_Get_VideoView() {
	var (
		id        = 1
		viewer_id = "1"
		viewed_at = time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)
	)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "video_view" WHERE (video_id = $1)`)).
		WithArgs(id).
		WillReturnRows(sqlmock.NewRows([]string{"video_id", "viewer_id", "viewed_at"}).
			AddRow(id, viewer_id, viewed_at))

	res, err := s.repository.Get(id)

	require.NoError(s.T(), err)
	require.Nil(s.T(), deep.Equal(&model.VideoView{VideoID: id, ViewerID: viewer_id, ViewedAt: viewed_at}, res))
}
